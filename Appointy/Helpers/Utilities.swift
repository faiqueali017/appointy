//
//  Utilities.swift
//  Appointy
//
//  Created by Faiq on 12/05/2021.
//

import Foundation
import UIKit

class Utilities {
    
    static func styleTextField(_ textfield:UITextField) {
        
        // Create the bottom line
        let bottomLine = CALayer()
        
        bottomLine.frame = CGRect(x: 0, y: textfield.frame.height - 2, width: textfield.frame.width - 20, height: 2)
        
        bottomLine.backgroundColor = UIColor(named: "Custom-Picture-Background")!.cgColor
        
        // Remove border on text field
        textfield.borderStyle = .none
        
        // Add the line to the text field
        textfield.layer.addSublayer(bottomLine)
        
    }
    
    static func styleFilledButton(_ button:UIButton) {
        
        // Filled rounded corner style
        button.backgroundColor = UIColor(named: "Custom-Picture-Background")
        button.layer.cornerRadius = 25.0
        button.tintColor = UIColor.white
    }
    
    static func styleHollowButton(_ button:UIButton) {
        
        // Hollow rounded corner style
        button.layer.borderWidth = 3
        button.layer.borderColor = UIColor(named: "Custom-Picture-Background")!.cgColor
        button.layer.cornerRadius = 25.0
        button.tintColor = UIColor(named: "Custom-Picture-Background")
    }
    
    static func isPasswordValid(_ password : String) -> Bool {
        
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[a-z])(?=.*[$@$#!%*?&])[A-Za-z\\d$@$#!%*?&]{8,}")
        return passwordTest.evaluate(with: password)
    }
    
    //Convert text field text into number
    static func integer(_ textField: UITextField) -> Int {
        guard let text = textField.text, let number = Int(text) else {return 0}
        return number
    }
    
    static func integer(_ label: UILabel) -> Int {
        guard let text = label.text, let number = Int(text) else {return 0}
        return number
    }
    
    //Convert label text into number
    static func double(_ label: UILabel) -> Double {
        guard let text = label.text, let number = Double(text) else {
            return 0
        }
        return number
    }
    
    static func getTotalEventCost(_ venueCostLabel: UILabel, refreshmentCostLabel: UILabel) -> Double {
        guard let venueCost = venueCostLabel.text, let refreshmentCost = refreshmentCostLabel.text, let num1 = Double(venueCost), let num2 = Double(refreshmentCost) else {
            return 0
        }
        return num1 + num2
    }
    
    static func convertDateTimeFromTimeStamp(timestamp: Int) -> String {
        //converting date
        let epocTime = TimeInterval(timestamp)
        let myDate = NSDate(timeIntervalSince1970: epocTime)

        //formatting date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM d, h:mm a"

        return dateFormatter.string(from: myDate as Date)
    }
    
    static func getUserID() -> String {
        var uid = String()
        
        if let data = UserDefaults.standard.data(forKey: Constants.USER_DEFAULTS.PATIENT_CREDENTIALS){
            do {
                let decoder = JSONDecoder()
                let p = try decoder.decode(UserModel.self, from: data)
                uid = p._id
            } catch {
                print("Unable to decode: \(error)")
            }
        }
        return uid
    }
    
}
