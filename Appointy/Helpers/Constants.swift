//
//  Constants.swift
//  Appointy
//
//  Created by Faiq on 16/04/2021.
//

import Foundation

struct Constants {
    
    struct STORYBOARD {
        static let TAB_BAR_CONTROLLER_ID = "MainTabBarController"
        static let LOGIN_NAVIGATION_CONTROLLER = "LoginNavigationController"
        static let SEARCH_DR_VIEW_CONTROLLER_ID = "SearchDrViewController"
        static let SEARCH_DR_SPECIALIZATION_CONTROLLER_ID = "SearchSpecializationViewController"
        static let SEARCH_SYMPTOMS_CONTROLLER_ID = "SearchSymptomsViewController"
        static let CALL_ASSIST_CONTROLLER_ID = "CallAssistViewController"
        static let FIND_MY_DR_CONTROLLER_ID = "FindMyDrViewController"
        static let SIGN_IN_CONTROLLER_ID = "SigninViewController"
        static let DOCTOR_DETAIL_CONTROLLER_ID = "DoctorDetailViewController"
        static let EDIT_ACCOUNT_DETAILS_CONTROLLER_ID = "EditAccountDetailsViewController"
    }
    
    struct USER_DEFAULTS {
        static let PATIENT_CREDENTIALS = "PATIENT_CREDENTIALS"
        
        struct KEYS {
            static let PATIENT_SIGNED_IN = "PATIENT_SIGNED_IN"
        }
    }
    
    struct APP_STATE_PROPERTIES {
        static let PATIENT_SIGNED_IN = "SUCCESSFULL"
    }
    
    struct ROUTES {
        static let SIGN_IN_PATIENT = "https://datamansys.herokuapp.com/api/v1/patient/login-patient"
        static let SIGN_OUT_PATIENT = "https://datamansys.herokuapp.com/api/v1/patient/logout-patient"
        static let SIGN_UP_PATIENT = "https://datamansys.herokuapp.com/api/v1/patient/"
        static let GET_ALL_DOCTORS = "https://datamansys.herokuapp.com/api/v1/patient/getAllDoctors"
        static let CREATE_APPOINTMENT = "https://datamansys.herokuapp.com/api/v1/appointment/"
        static let UPDATE_CREDENTIALS = "https://datamansys.herokuapp.com/api/v1/patient/"
        static let GET_ALL_APPOINTMENTS = "https://datamansys.herokuapp.com/api/v1/patient/getmydoctors"
    }
    
    struct NOTIFICATIONS {
        static let DOCTORS_FETCHED = "DOCTORS_FETCHED"
        static let DISMISS_DOCTORS_SCREEN = "DISMISS_DOCTORS_SCREEN"
        static let CREDENTIALS_UPDATED = "CREDENTIALS_UPDATED"
        static let APPOINTMENTS_FETCHED = "APPOINTMENTS_FETCHED"
        static let NEW_APPOINTMENT_CREATED = "NEW_APPOINTMENT_CREATED"
    }
}
