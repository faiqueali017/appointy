//
//  UIView+.swift
//  Appointy
//
//  Created by Faiq on 20/04/2021.
//

import UIKit

extension UIView {
    func dropShadow(scale: Bool = true, shadowOpacity: Float, shadowRadius: CGFloat, shadowColor: CGColor) {
        layer.masksToBounds = false
        layer.shadowColor = shadowColor
        layer.shadowOpacity = shadowOpacity
        layer.shadowOffset = .zero
        layer.shadowRadius = shadowRadius
        
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}
