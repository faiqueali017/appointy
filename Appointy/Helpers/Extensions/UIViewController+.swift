//
//  UIViewController+.swift
//  Appointy
//
//  Created by Faiq on 20/05/2021.
//

import Foundation
import UIKit

//MARK:- UIAvtivity incidcator extension
fileprivate var aView: UIView?

extension UIViewController {
    func showLoadingSpinner(){
        aView = UIView(frame: self.view.bounds)
        aView?.backgroundColor = UIColor(white: 0.1, alpha: 0.4)
        
        var indicator = UIActivityIndicatorView()
        if #available(iOS 13.0, *) {
            indicator = UIActivityIndicatorView(style: .large)
        }
        indicator.color = .white
        indicator.center = aView!.center
        indicator.startAnimating()
        aView?.addSubview(indicator)
        self.view.addSubview(aView!)
        
    }
    
    func removeLoadingSpinner(){
        aView?.removeFromSuperview()
        aView = nil
    }
}

//MARK:- Keyboard extension
extension UIViewController {
    
    //To Hide keyboard when tapped around
    func hideKeyboardWhenTappedAround(){
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
