//
//  CharacterSet+.swift
//  Appointy
//
//  Created by Faiq on 14/05/2021.
//

import Foundation

extension CharacterSet {
    static let urlQueryValueAllowed: CharacterSet = {
       let generalDelimitersToEncode = ":#[]@"
        let subDelimitersToEncode = "!$&'()*+,;="
        
        var allowed = CharacterSet.urlQueryAllowed
        allowed.remove(charactersIn: "\(generalDelimitersToEncode)\(subDelimitersToEncode)")
        return allowed
    }()
}
