//
//  Routes.swift
//  Appointy
//
//  Created by Faiq on 14/05/2021.
//

import Foundation
import UIKit

class Routes {
    
    //MARK:- Variables
    private let httpCookie = HTTPCookie()
    
    var arrDoctors = [Doc]()
    var arrAppointments = [Appointments]()
    
    weak var vc: SearchDrViewController?
    weak var vc1: SearchSpecializationViewController?
    weak var vc2: FindMyDrViewController?
    weak var vc3: MyAppointmentsViewController?

    
    //MARK:- Helper Methods
    /*
     ------------------------------------------------
     SIGN-IN PATIENT
     ------------------------------------------------
     */
    func signinPatient(_ email: String?, _ password: String?, onCompletion: @escaping (_ isSignedIn: Bool) -> Void){
        guard let email = email else {return}
        guard let password = password else {return}
        
        if let url = URL(string: Constants.ROUTES.SIGN_IN_PATIENT) {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            let parameters: [String: Any] = [
                "email": email,
                "password": password
            ]
            
            request.httpBody = parameters.percentEscaped().data(using: .utf8)
            
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                guard let data = data else {
                    if error == nil {
                        print(error?.localizedDescription ?? "Unknown error")
                    }
                    return
                }
                
                if let response = response as? HTTPURLResponse {
                    guard (200 ... 299) ~= response.statusCode else {
                        //print("Status code :- \(response.statusCode)")
                        print(response)
                        onCompletion(false)
                        return
                    }
                }
                
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    print(json)
                    
                    let patient = try JSONDecoder().decode(PatientModel.self, from: data)
                    
                    do {
                        if let name = patient.data?.user?.name, let email = patient.data?.user?.email, let role = patient.data?.user?.role, let active = patient.data?.user?.active, let id = patient.data?.user?._id, let contactNo = patient.data?.user?.contactNumber, let version = patient.data?.user?.__v {
                            
                            let user = UserModel(role: role,
                                                 active: active,
                                                 _id: id,
                                                 name: name,
                                                 email: email,
                                                 contactNumber: contactNo,
                                                 __v: version)
                            
                            //Saving patient credentials to user defaults
                            let encoder = JSONEncoder()
                            if let encoded = try? encoder.encode(user) {
                                UserDefaults.standard.setValue(encoded, forKey: Constants.USER_DEFAULTS.PATIENT_CREDENTIALS)
                            }
                        }
                        
                        //save user signed state to user defaults
                        UserDefaults.standard.set(Constants.APP_STATE_PROPERTIES.PATIENT_SIGNED_IN, forKey: Constants.USER_DEFAULTS.KEYS.PATIENT_SIGNED_IN)
                    } catch {
                        print("Unable to save \(error)")
                    }
                    
                    //read cookie from the req url
                    let cookie = self.httpCookie.readCookie(forURL: url)
                    
                    //storing cookies
                    self.httpCookie.storeCookies(cookie, forURL: url)
                    
                    //
                    onCompletion(true)
                } catch {
                    print(error.localizedDescription)
                    onCompletion(false)
                }
            }.resume()
            
        }
    }
    
    
    /*
     ------------------------------------------------
     SIGN-UP PATIENT
     ------------------------------------------------
     */
    func signupPatient(_ name: String?, _ email: String?, _ contactNumber: String?, _ password: String?, _ confirmPassword: String?, onCompletion: @escaping (_ isSignedUp: Bool) -> Void){
        guard let name = name else {return}
        guard let email = email else {return}
        guard let contactNo = contactNumber else {return}
        guard let password = password else {return}
        guard let confirmPassword = confirmPassword else {return}

        if let url = URL(string: Constants.ROUTES.SIGN_UP_PATIENT) {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"

            let parameters: [String: Any] = [
                "name": name,
                "email": email,
                "contactNumber": contactNo,
                "password": password,
                "passwordConfirm": confirmPassword
            ]

            request.httpBody = parameters.percentEscaped().data(using: .utf8)

            URLSession.shared.dataTask(with: request) { (data, response, error) in
                guard let data = data else {
                    if error == nil {
                        print(error?.localizedDescription ?? "Unknown error")
                    }
                    return
                }

                if let response = response as? HTTPURLResponse {
                    guard (200 ... 299) ~= response.statusCode else {
                        print("Status code :- \(response.statusCode)")
                        print(response)
                        onCompletion(false)
                        return
                    }
                }

                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    print(json)
                    
                    //
                    onCompletion(true)
                } catch {
                    print(error.localizedDescription)
                    onCompletion(false)
                }
            }.resume()
        }
    }
    
    
    /*
     ------------------------------------------------
     SIGN-OUT PATIENT
     ------------------------------------------------
     */
    func signoutPatient(onCompletion: @escaping (_ isSignedOut: Bool) -> Void){
        guard let url = URL(string: Constants.ROUTES.SIGN_OUT_PATIENT) else {return}
        
        let session = URLSession.shared
        session.dataTask(with: url) { (data, response, error) in
            
            let httpRes: HTTPURLResponse = (response as? HTTPURLResponse)!
            let cookies: [HTTPCookie] = HTTPCookie.cookies(withResponseHeaderFields: httpRes.allHeaderFields as! [String: String], for: httpRes.url!)
            HTTPCookieStorage.shared.setCookies(cookies, for: response?.url!, mainDocumentURL: nil)
            
            if let response = response {
                //print(response)
            }
            
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    print(json)
                    onCompletion(true)
                } catch {
                    print(error.localizedDescription)
                }
            }
        }.resume()
    }
    
    
    /*
     ------------------------------------------------
     GET ALL DOCTORS
     ------------------------------------------------
     */
    func getAllDoctors(){
        guard let url = URL(string: Constants.ROUTES.GET_ALL_DOCTORS) else {return}
        
        let session = URLSession.shared
        session.dataTask(with: url) { (data, response, error) in
            
            let httpRes: HTTPURLResponse = (response as? HTTPURLResponse)!
            let cookies: [HTTPCookie] = HTTPCookie.cookies(withResponseHeaderFields: httpRes.allHeaderFields as! [String: String], for: httpRes.url!)
            HTTPCookieStorage.shared.setCookies(cookies, for: response?.url!, mainDocumentURL: nil)
            
            if let response = response {
                //print(response)
            }
            
            if error == nil {
                if let data = data {
                    do {
                        let json = try JSONDecoder().decode(DoctorModel.self, from: data)
                        
                        if let doctors = json.data?.doc {
                            self.arrDoctors.append(contentsOf: doctors)
                        }
                        
                        //Trigger notification here
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.NOTIFICATIONS.DOCTORS_FETCHED), object: nil)
                        
                        DispatchQueue.main.async {
                            self.vc?.drTableView.reloadData()
                            self.vc1?.specializationTableView.reloadData()
                            self.vc2?.tableView.reloadData()
                        }
                    } catch {
                        print(error.localizedDescription)
                    }
                }
            }
        }.resume()
    }
    
    
    /*
     ------------------------------------------------
     CREATE AN APPOINTMENT
     ------------------------------------------------
     */
    func createAppointment(_ doctorID: String?, onCompletion: @escaping (_ isCreated: Bool) -> Void){
        guard let doctorID = doctorID else {return}
        
        if let url = URL(string: Constants.ROUTES.CREATE_APPOINTMENT) {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            let parameters: [String: Any] = [
                "doctorId": doctorID
            ]
            
            request.httpBody = parameters.percentEscaped().data(using: .utf8)
            
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                if let response = response as? HTTPURLResponse {
                    guard (200 ... 299) ~= response.statusCode else {
                        //print("Status code :- \(response.statusCode)")
                        print(response.statusCode)
                        return
                    }
                }
                
                if let data = data {
                    do {
                        let json = try JSONSerialization.jsonObject(with: data, options: [])
                        print(json)
                        
                        //read cookie from the req url
                        let cookie = self.httpCookie.readCookie(forURL: url)
                        self.httpCookie.storeCookies(cookie, forURL: url)
                        
                        //
                        onCompletion(true)
                    } catch {
                        print(error)
                    }
                }
            }.resume()
        }
    }
    
    /*
     ------------------------------------------------
     UPDATE CREDENTIALS
     ------------------------------------------------
     */
    func updateCredentials(_ name: String?, _ email: String?, _ contactNumber: String?, onCompletion: @escaping (_ isUpdated: Bool) -> Void){
        guard let name = name else {return}
        guard let email = email else {return}
        guard let contactNumber = contactNumber else {return}
        
        let urlString = Constants.ROUTES.UPDATE_CREDENTIALS + Utilities.getUserID()
        if let url = URL(string: urlString) {
            var request = URLRequest(url: url)
            
            request.httpMethod = "PATCH"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            
            do {
                let parameters: [String: Any] = [
                    "name": name,
                    "email": email,
                    "contactNumber": contactNumber
                ]
                
                let jsonData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
                request.httpBody = jsonData
                //print("JSON Data: ", String(data: request.httpBody!, encoding: .utf8) ?? "no body data")
                
                //read cookie from the req url while making req
                let cookie = self.httpCookie.readCookie(forURL: url)
                self.httpCookie.storeCookies(cookie, forURL: url)

            } catch {
                print("Error")
            }
            
            
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                if error != nil {
                    print(error!)
                    onCompletion(false)
                    return
                }
                
                if let response = response as? HTTPURLResponse {
                    guard (200 ... 299) ~= response.statusCode else {
                        print("Status code :- \(response.statusCode)")
                        //onCompletion(true)
                        return
                    }
                }
                
                if let data = data {
                    do {
                        let json = try JSONSerialization.jsonObject(with: data, options: [])
                        print(json)
                        
                        let patient = try JSONDecoder().decode(UpdateCredentialModel.self, from: data)
                        
                        do {
                            if let name = patient.data?.doc?.name, let email = patient.data?.doc?.email, let role = patient.data?.doc?.role, let active = patient.data?.doc?.active, let id = patient.data?.doc?._id, let contactNo = patient.data?.doc?.contactNumber, let version = patient.data?.doc?.__v {

                                let user = UserModel(role: role,
                                                     active: active,
                                                     _id: id,
                                                     name: name,
                                                     email: email,
                                                     contactNumber: contactNo,
                                                     __v: version)
                                
                                //Updating patient credentials to user defaults
                                let encoder = JSONEncoder()
                                if let encoded = try? encoder.encode(user) {
                                    UserDefaults.standard.setValue(encoded, forKey: Constants.USER_DEFAULTS.PATIENT_CREDENTIALS)
                                }
                                
                                //
                                onCompletion(true)
                            }
                            
                        } catch {
                            print("Unable to update in user defaults: \(error)")
                        }
                    } catch {
                        print(error)
                    }
                }
                
            }.resume()
        }
    }
    
    /*
     ------------------------------------------------
     GET ALL APPOINTMENTS
     ------------------------------------------------
     */
    func getAllAppointments(){
        guard let url = URL(string: Constants.ROUTES.GET_ALL_APPOINTMENTS) else {return}
        
        let session = URLSession.shared
        session.dataTask(with: url) { (data, response, error) in
            
            let httpRes: HTTPURLResponse = (response as? HTTPURLResponse)!
            let cookies: [HTTPCookie] = HTTPCookie.cookies(withResponseHeaderFields: httpRes.allHeaderFields as! [String: String], for: httpRes.url!)
            HTTPCookieStorage.shared.setCookies(cookies, for: response?.url!, mainDocumentURL: nil)
            
            if let response = response {
                print(response)
            }
            
            if error == nil {
                if let data = data {
                    do {
                        let json = try JSONDecoder().decode(DoctorAppointmentsModel.self, from: data)
                        print(json)
                        
                        if let appointment = json.appointments {
                            self.arrAppointments.append(contentsOf: appointment)
                        }
                        
                        //Trigger notification here
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.NOTIFICATIONS.APPOINTMENTS_FETCHED), object: nil)
                        
                        DispatchQueue.main.async {
                            self.vc3?.appointmentTableView.reloadData()
                        }
                    } catch {
                        print(error.localizedDescription)
                    }
                }
            }
        }.resume()
    }
    
    
}
