//
//  UserModel.swift
//  Appointy
//
//  Created by Faiq on 21/05/2021.
//

import Foundation

struct UserModel: Codable {
    let role: String
    let active: Bool
    let _id: String
    let name: String
    let email: String
    let contactNumber: String
    let __v: Int
}
