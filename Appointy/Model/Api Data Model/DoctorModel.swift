//
//  DoctorModel.swift
//  Appointy
//
//  Created by Faiq on 15/05/2021.
//

import Foundation

struct DoctorModel : Codable {
    let status : String?
    let results : Int?
    let data : DrData?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case results = "results"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        results = try values.decodeIfPresent(Int.self, forKey: .results)
        data = try values.decodeIfPresent(DrData.self, forKey: .data)
    }

}

struct Doc : Codable {
    let role : String?
    let _id : String?
    let name : String?
    let email : String?
    let experience : Int?
    let speciality : String?

    enum CodingKeys: String, CodingKey {
        case role = "role"
        case _id = "_id"
        case name = "name"
        case email = "email"
        case experience = "experience"
        case speciality = "speciality"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        role = try values.decodeIfPresent(String.self, forKey: .role)
        _id = try values.decodeIfPresent(String.self, forKey: ._id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        experience = try values.decodeIfPresent(Int.self, forKey: .experience)
        speciality = try values.decodeIfPresent(String.self, forKey: .speciality)
    }

}

struct DrData : Codable {
    let doc : [Doc]?

    enum CodingKeys: String, CodingKey {
        case doc = "doc"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        doc = try values.decodeIfPresent([Doc].self, forKey: .doc)
    }

}
