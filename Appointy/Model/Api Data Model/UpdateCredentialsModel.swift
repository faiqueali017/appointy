//
//  UpdateCredentialsModel.swift
//  Appointy
//
//  Created by Faiq on 21/05/2021.
//

import Foundation

struct UpdateCredentialModel: Codable {
    let status: String?
    let message: String?
    let data: Data1?
    
    enum CodingKeys: String, CodingKey {
        case status = "status"
        case message = "message"
        case data = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(Data1.self, forKey: .data)
    }
    
}

struct Data1: Codable {
    let doc: User?
    
    enum CodingKeys: String, CodingKey {
        case doc = "doc"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        doc = try values.decodeIfPresent(User.self, forKey: .doc)
    }
    
}
