//
//  SigninPatient.swift
//  Appointy
//
//  Created by Faiq on 12/05/2021.
//

import Foundation

struct PatientModel: Codable {
    let status: String?
    let token: String?
    let data: Data?
    
    enum CodingKeys: String, CodingKey {
        case status = "status"
        case token = "token"
        case data = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        token = try values.decodeIfPresent(String.self, forKey: .token)
        data = try values.decodeIfPresent(Data.self, forKey: .data)
    }
    
}

struct Data: Codable {
    let user: User?
    
    enum CodingKeys: String, CodingKey {
        case user = "user"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        user = try values.decodeIfPresent(User.self, forKey: .user)
    }
    
}

struct User: Codable {
    let role: String?
    let active: Bool?
    let _id: String?
    let name: String?
    let email: String?
    let contactNumber: String?
    let __v: Int?
    
    enum CodingKeys: String, CodingKey {
        case role = "role"
        case active = "Active"
        case _id = "_id"
        case name = "name"
        case email = "email"
        case contactNumber = "contactNumber"
        case __v = "__v"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        role = try values.decodeIfPresent(String.self, forKey: .role)
        active = try values.decodeIfPresent(Bool.self, forKey: .active)
        _id = try values.decodeIfPresent(String.self, forKey: ._id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        contactNumber = try values.decodeIfPresent(String.self, forKey: .contactNumber)
        __v = try values.decodeIfPresent(Int.self, forKey: .__v)
    }
    
}

