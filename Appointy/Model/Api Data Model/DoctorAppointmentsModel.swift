//
//  DoctorAppointmentsModel.swift
//  Appointy
//
//  Created by Faiq on 22/05/2021.
//

import Foundation

struct DoctorAppointmentsModel : Codable {
    let total : Int?
    let appointments : [Appointments]?

    enum CodingKeys: String, CodingKey {

        case total = "total"
        case appointments = "appointments"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        total = try values.decodeIfPresent(Int.self, forKey: .total)
        appointments = try values.decodeIfPresent([Appointments].self, forKey: .appointments)
    }

}

struct Appointments : Codable {
    let _id : String?
    let patient : User?
    let doctor : Doc?
    let date : String?
    let __v : Int?

    enum CodingKeys: String, CodingKey {

        case _id = "_id"
        case patient = "patient"
        case doctor = "doctor"
        case date = "date"
        case __v = "__v"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        _id = try values.decodeIfPresent(String.self, forKey: ._id)
        patient = try values.decodeIfPresent(User.self, forKey: .patient)
        doctor = try values.decodeIfPresent(Doc.self, forKey: .doctor)
        date = try values.decodeIfPresent(String.self, forKey: .date)
        __v = try values.decodeIfPresent(Int.self, forKey: .__v)
    }

}
