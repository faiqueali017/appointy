//
//  AppointmentTableViewCell.swift
//  Appointy
//
//  Created by Faiq on 21/05/2021.
//

import UIKit

class AppointmentTableViewCell: UITableViewCell {
    
    static let reuseIdentifier = "AppointmentTableViewCell"

    @IBOutlet weak var startTimeLabel: UILabel!
    @IBOutlet weak var endTimeLabel: UILabel!
    @IBOutlet weak var startTimeDot: UIView!
    @IBOutlet weak var endTimeDot: UIView!
    @IBOutlet weak var timeThreadView: UIView!
    
    @IBOutlet weak var detailsBackgroundView: UIView!
    @IBOutlet weak var detailsSideView: UIView!
    @IBOutlet weak var doctorNameLabel: UILabel!
    @IBOutlet weak var doctorSpecialityLabel: UILabel!
    @IBOutlet weak var doctorAddressLabel: UILabel!
    @IBOutlet weak var locationMarkImageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        startTimeDot.layer.cornerRadius = 6.5/2
        endTimeDot.layer.cornerRadius = 6.5/2
        
        locationMarkImageView.image = UIImage(named: "location-mark")?.withRenderingMode(.alwaysTemplate)
        locationMarkImageView.tintColor = UIColor(named: "Custom-Indigo-1")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
