//
//  NoDrFoundTableViewCell.swift
//  Appointy
//
//  Created by Faiq on 21/05/2021.
//

import UIKit

class NoDrFoundTableViewCell: UITableViewCell {
    
    static let reuseIdentifier = "NoDrFoundTableViewCell"

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureDescriptionLabelForNoResultsFound(){
        titleLabel.text = "No Doctor Found !!"
        descriptionLabel.text = "The doctor/specialization which you are trying to search doesn't appear. Try searching some other or you can avail our 'Call Assist'."
    }
    
    func configureDescriptionLabelForNoAppointments(){
        titleLabel.text = "No Appointment Yet !!"
        descriptionLabel.text = "You don't have any appointments in progress for now. Go to 'Book Appointment' to create an appointment."
    }
    
}
