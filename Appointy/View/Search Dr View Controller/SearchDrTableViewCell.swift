//
//  SearchDrTableViewCell.swift
//  Appointy
//
//  Created by Faiq on 16/04/2021.
//

import UIKit

class SearchDrTableViewCell: UITableViewCell {
    
    static let reuseIdentifier = "SearchDrTableViewCell"
    
    @IBOutlet weak var doctorNameLabel: UILabel!
    @IBOutlet weak var doctorImageBackgroundView: UIView!
    @IBOutlet weak var doctorImageView: UIImageView!
    @IBOutlet weak var doctorSpecializationLabel: UILabel!
    @IBOutlet weak var doctorRatingImage: UIImageView!
    @IBOutlet weak var doctorRatingLabel: UILabel!
    @IBOutlet weak var doctorBackgroundView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        doctorImageBackgroundView.layer.cornerRadius = 62/2
        doctorImageView.layer.cornerRadius = 59/2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
