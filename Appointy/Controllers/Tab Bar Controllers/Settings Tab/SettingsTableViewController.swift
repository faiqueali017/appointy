//
//  SettingsTableViewController.swift
//  Appointy
//
//  Created by Faiq on 20/05/2021.
//

import UIKit

class SettingsTableViewController: UITableViewController {
    
    //MARK:- IBOutlets
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var profilePicImageView: UIImageView!
    
    //MARK:- Variables
    private let httpCookie = HTTPCookie()
    private let routes = Routes()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        profilePicImageView.layer.cornerRadius = 75/2
        getUserCredentials()
        //Notification when all doctors are fetched
        NotificationCenter.default.addObserver(self, selector: #selector(getUserCredentials), name: NSNotification.Name(rawValue: Constants.NOTIFICATIONS.CREDENTIALS_UPDATED), object: nil)
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 && indexPath.row == 0 {
            print("Profile edit")
            tableView.deselectRow(at: indexPath, animated: true)
        }
        else if indexPath.section == 1 && indexPath.row == 0 {
            tableView.deselectRow(at: indexPath, animated: true)
            transitionToEditAccountDetails()
        }
        
        else if indexPath.section == 3 && indexPath.row == 0 {
            showLogoutAlert()
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    //MARK:- Helper Methods
    private func showLogoutAlert(){
        //create alert
        let alert = UIAlertController(title: "Sign out", message: "Are you sure you want to sign out? You can always access your content by signing back in.", preferredStyle: UIAlertController.Style.alert)
        
        //add action to alert
        alert.addAction(UIAlertAction(title: "Confirm", style: .default) { (alertAction) in
            self.showLoadingSpinner()
            self.logoutAndDeleteData { (isDeleted) in
                if isDeleted {
                    DispatchQueue.main.async {
                        self.removeLoadingSpinner()
                        self.transitionToLoginScreen()
                    }
                } else {
                    print("Error in signing out")
                }
            }
        })
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        //show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    private func logoutAndDeleteData(onCompletion: @escaping (_ isDeleted: Bool) -> Void){
        //signout patient
        routes.signoutPatient { (isSignedOut) in
            if isSignedOut {
                //clear user defaults
                UserDefaults.standard.removeObject(forKey: Constants.USER_DEFAULTS.PATIENT_CREDENTIALS)
                UserDefaults.standard.removeObject(forKey: Constants.USER_DEFAULTS.KEYS.PATIENT_SIGNED_IN)

                //delete stored cookies
                self.httpCookie.deleteCookie(forURL: URL(string: Constants.ROUTES.SIGN_IN_PATIENT)!)

                onCompletion(true)
            } else {
                print("Error signing out")
            }
        }
    }
    
    private func transitionToLoginScreen(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let loginNavController = storyboard.instantiateViewController(identifier: Constants.STORYBOARD.LOGIN_NAVIGATION_CONTROLLER)
        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(loginNavController)
    }
    
    private func transitionToEditAccountDetails(){
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: Constants.STORYBOARD.EDIT_ACCOUNT_DETAILS_CONTROLLER_ID)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc private func getUserCredentials(){
        if let data = UserDefaults.standard.data(forKey: Constants.USER_DEFAULTS.PATIENT_CREDENTIALS) {
            do {
                let decoder = JSONDecoder()
                let p = try decoder.decode(UserModel.self, from: data)
                self.usernameLabel.text = p.name
            } catch {
                print("Unable to decode: \(error)")
            }
        }
    }
}


