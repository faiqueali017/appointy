//
//  EditAccountDetailsViewController.swift
//  Appointy
//
//  Created by Faiq on 20/05/2021.
//

import UIKit

class EditAccountDetailsViewController: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var saveBarButton: UIBarButtonItem!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneNoTextField: UITextField!
    
    //MARK:- Variables
    private var username = String()
    private var email = String()
    private var phoneNo = String()
    
    private var isNameChanged = Bool()
    private var isEmailChanged = Bool()
    private var isPhonoNoChanged = Bool()
    
    private let routes = Routes()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        saveBarButton.isEnabled = false
        hideKeyboardWhenTappedAround()
        getUserCredentials()
    }
    
    //MARK:- IBActions
    @IBAction func didTapSaveButton(_ sender: UIBarButtonItem) {
        routes.updateCredentials(nameTextField.text, emailTextField.text, phoneNoTextField.text) { (isUpdated) in
            if isUpdated {
                DispatchQueue.main.async {
                    self.presentUpdateHudAndDismissScreen()
                }
            } else {
                print("Error in updating")
            }
        }
    }
    
    @IBAction func nameFieldDidChanged(_ sender: UITextField) {
        if sender.text == username {
            saveBarButton.isEnabled = false
            isNameChanged = false
        } else {
            saveBarButton.isEnabled = true
            isNameChanged = true
        }
    }
    
    @IBAction func emailFieldDidChanged(_ sender: UITextField) {
        if sender.text == email {
            saveBarButton.isEnabled = false
            isEmailChanged = false
        } else {
            saveBarButton.isEnabled = true
            isEmailChanged = true
        }
    }
    @IBAction func contactNoTextFieldDidChanged(_ sender: UITextField) {
        if sender.text == phoneNo {
            saveBarButton.isEnabled = false
            isPhonoNoChanged = false
        } else {
            saveBarButton.isEnabled = true
            isPhonoNoChanged = true
        }
    }
    
    //MARK:- Helper Methods
    private func getUserCredentials(){
        if let data = UserDefaults.standard.data(forKey: Constants.USER_DEFAULTS.PATIENT_CREDENTIALS) {
            do {
                let decoder = JSONDecoder()
                let p = try decoder.decode(UserModel.self, from: data)
                self.nameTextField.text = p.name
                self.emailTextField.text = p.email
                self.phoneNoTextField.text = p.contactNumber
                
                self.username = p.name
                self.email = p.email
                self.phoneNo = p.contactNumber
            } catch {
                print("Unable to decode: \(error)")
            }
        }
    }
    
    private func presentUpdateHudAndDismissScreen(){
        //Initialize a Hud view obj
        let hudView = HudView.hud(inView: self.view, animated: true)
        
        //Set hud view text
        hudView.text = NSLocalizedString("Updated", comment: "Updated")
        
        //Dismiss hud view timings
        hudView.afterDelay(0.7) {
            hudView.hide()
            self.navigationController?.popViewController(animated: true)
            
            //Trigger notification here
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.NOTIFICATIONS.CREDENTIALS_UPDATED), object: nil)
        }
    }
}
