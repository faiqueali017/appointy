//
//  DoctorDetailViewController.swift
//  Appointy
//
//  Created by Faiq on 19/05/2021.
//

import UIKit
import MapKit

class DoctorDetailViewController: UIViewController {
    
    private let scrollView: UIScrollView = {
        let view = UIScrollView()
        view.translatesAutoresizingMaskIntoConstraints = false
        //view.showsVerticalScrollIndicator = false
        //view.backgroundColor = .systemBackground
        return view
    }()
    
    /// DOCTOR HEADER VIEW
    private let doctorHeaderView: UIView = {
        let view  = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 13.0, *) {
            view.backgroundColor = .systemBackground
        } else {
            view.backgroundColor = .white
        }
        return view
    }()
    
    private let doctorClinicMap: MKMapView = {
        let map = MKMapView()
        map.translatesAutoresizingMaskIntoConstraints = false
        return map
    }()
    
    private let doctorHeaderImageView: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.image = UIImage(named: "clinic-blur-2")
        return image
    }()
    
    private let doctorImageBackgroundView: UIView = {
        let view  = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 13.0, *) {
            view.backgroundColor = .systemBackground
        } else {
            view.backgroundColor = .white
        }
        return view
    }()
    
    private let doctorImageView: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.image = UIImage(named: "Doctor1")
        image.layer.masksToBounds = false
        image.clipsToBounds = true
        image.contentMode = .scaleAspectFill
        return image
    }()
    
    /// DOCTOR MID VIEW
    private let doctorMidView: UIView = {
        let view  = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 13.0, *) {
            view.backgroundColor = .systemBackground
        } else {
            view.backgroundColor = .white
        }
        return view
    }()
    
    let doctorNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Avenir Next Bold", size: 30)
        label.text = "Turtle Park"
        label.numberOfLines = 0
        //label.backgroundColor = .yellow
        if #available(iOS 13.0, *) {
            label.textColor = .label
        } else {
            label.textColor = .black
        }
        return label
    }()
    
    let doctorSpecialityLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Avenir Next", size: 21)
        label.text = "Joshua Tree National park, California"
        label.numberOfLines = 0
        if #available(iOS 13.0, *) {
            label.textColor = UIColor.systemGray
        } else {
            label.textColor = .black
        }
        //label.backgroundColor = .red
        return label
    }()
    
    let doctorMidViewSeparatorLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = .lightGray
        return label
    }()
    
    /// DOCTOR FOOTEER VIEW
    private let doctorFooterView: UIView = {
        let view  = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 13.0, *) {
            view.backgroundColor = .systemBackground
        } else {
            view.backgroundColor = .white
        }
        return view
    }()
    
    private let timingsIcon: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        let img = UIImage(named: "timings-icon")?.withRenderingMode(.alwaysTemplate)
        image.image = img
        image.tintColor = UIColor(named: "White-Black")
        return image
    }()
    
    private let timingsLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Avenir", size: 17)
        label.text = "10:00 AM - 10:00PM"
        label.numberOfLines = 0
        label.textColor = .label
        return label
    }()
    
    private let chargesIcon: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        let img = UIImage(named: "cost-icon")?.withRenderingMode(.alwaysTemplate)
        image.image = img
        image.tintColor = UIColor(named: "White-Black")
        return image
    }()
    
    private let chargesLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Avenir", size: 17)
        label.text = "$10"
        label.numberOfLines = 0
        label.textColor = .label
        return label
    }()
    
    private let emailIcon: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        let img = UIImage(named: "email-icon")?.withRenderingMode(.alwaysTemplate)
        image.image = img
        image.tintColor = UIColor(named: "White-Black")
        return image
    }()
    
    private let doctorEmailLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Avenir", size: 17)
        label.text = "lorem@appointy.com"
        label.numberOfLines = 0
        label.textColor = .label
        return label
    }()
    
    private let doctorContactIcon: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        let img = UIImage(named: "phone-icon")?.withRenderingMode(.alwaysTemplate)
        image.image = img
        image.tintColor = UIColor(named: "White-Black")
        return image
    }()

    private let doctorContactLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Avenir", size: 17)
        label.text = "+92-3333333333"
        label.numberOfLines = 0
        label.textColor = .label
        return label
    }()
    
    ///BOOK YOUR SLOTS VIEW
    private let appointmentBookingSlotView: UIView = {
        let view  = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 13.0, *) {
            view.backgroundColor = .systemBackground
        } else {
            view.backgroundColor = .white
        }
        return view
    }()
    
    private let bookYourSlotsLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Avenir Next Bold", size: 22)
        label.text = "Book your appointment"
        label.numberOfLines = 0
        if #available(iOS 13.0, *) {
            label.textColor = .label
        } else {
            label.textColor = .black
        }
        return label
    }()
    
    private let bookYourSlotsDescLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Avenir", size: 17)
        label.text = "Check availability of appointment on your desired date and time."
        label.numberOfLines = 0
        label.textColor = .label
        return label
    }()
    
    private let datePicker: UIDatePicker = UIDatePicker()
    
    private let checkAvailabilityLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Avenir", size: 17)
        label.text = "The appointment slot is available on your selected date and time."
        label.numberOfLines = 0
        label.textAlignment = .center
        label.textColor = .systemGreen
        label.isHidden = true
        return label
    }()
    
    private let checkAvailabilityButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Confirm Appointment", for: .normal)
        button.backgroundColor = UIColor(named: "Custom-Btn-1")
        button.titleLabel?.textColor = .white
        button.titleLabel?.font = UIFont(name: "Avenir Next", size: 22)
        button.addTarget(self, action: #selector(didTapCheckAvailabilityButton), for: .touchUpInside)
        button.isHidden = false
        return button
    }()
    
    //MARK:- Variables
    var doctorName = String()
    var doctorSpeciality = String()
    var doctorEmail = String()
    var doctorId = String()
    private var doctorsModel = Routes()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(scrollView)
        scrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        configureDoctorHeaderViewUI()
        
        configureDoctorMidViewUI()
        
        configureDoctorFooterViewUI()
        
        configureDoctorBookingSlotsViewUI()
        
        configureScreenLabels()
    }
    
    private func configureDoctorHeaderViewUI(){
        scrollView.addSubview(doctorHeaderView)
        doctorHeaderView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        doctorHeaderView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        doctorHeaderView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        doctorHeaderView.heightAnchor.constraint(equalToConstant: 360).isActive = true
        
        ///ADDING MAP
//        doctorHeaderView.addSubview(doctorClinicMap)
//        doctorClinicMap.topAnchor.constraint(equalTo: doctorHeaderView.topAnchor).isActive = true
//        doctorClinicMap.leadingAnchor.constraint(equalTo: doctorHeaderView.leadingAnchor).isActive = true
//        doctorClinicMap.trailingAnchor.constraint(equalTo: doctorHeaderView.trailingAnchor).isActive = true
//        doctorClinicMap.heightAnchor.constraint(equalToConstant: 270).isActive = true
        
        //configureVenueMap()
        
        doctorHeaderView.addSubview(doctorHeaderImageView)
        doctorHeaderImageView.topAnchor.constraint(equalTo: doctorHeaderView.topAnchor).isActive = true
        doctorHeaderImageView.leadingAnchor.constraint(equalTo: doctorHeaderView.leadingAnchor).isActive = true
        doctorHeaderImageView.trailingAnchor.constraint(equalTo: doctorHeaderView.trailingAnchor).isActive = true
        doctorHeaderImageView.heightAnchor.constraint(equalToConstant: 220).isActive = true
        
        ///ADDING VENUE IMAGE-BACKGROUND
        doctorHeaderView.addSubview(doctorImageBackgroundView)
        doctorImageBackgroundView.heightAnchor.constraint(equalToConstant: 250).isActive = true
        doctorImageBackgroundView.widthAnchor.constraint(equalToConstant: 250).isActive = true
        doctorImageBackgroundView.centerXAnchor.constraint(equalTo: doctorHeaderView.centerXAnchor).isActive = true
        doctorImageBackgroundView.centerYAnchor.constraint(equalTo: doctorHeaderImageView.bottomAnchor).isActive = true
        doctorImageBackgroundView.layer.cornerRadius = 250/2
        
        //Add background view shadow
        doctorImageBackgroundView.dropShadow(shadowOpacity: 0.3, shadowRadius: 12, shadowColor: UIColor(named: "White-Black")!.cgColor)
        
        
        ///ADDING VENUE IMAGE
        doctorImageBackgroundView.addSubview(doctorImageView)
        doctorImageView.heightAnchor.constraint(equalToConstant: 240).isActive = true
        doctorImageView.widthAnchor.constraint(equalToConstant: 240).isActive = true
        doctorImageView.centerXAnchor.constraint(equalTo: doctorImageBackgroundView.centerXAnchor).isActive = true
        doctorImageView.centerYAnchor.constraint(equalTo: doctorImageBackgroundView.centerYAnchor).isActive = true
        doctorImageView.layer.cornerRadius = 240/2
        
    }
    
    private func configureVenueMap(){
        let initialLocation = CLLocation(latitude: 21.282778, longitude: -157.829444)
        doctorClinicMap.centerToLocation(initialLocation)
    }
    
    private func configureDoctorMidViewUI(){
        
        scrollView.addSubview(doctorMidView)
        doctorMidView.topAnchor.constraint(equalTo: doctorHeaderView.bottomAnchor).isActive = true
        doctorMidView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        doctorMidView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        doctorMidView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        ///ADDING VENUE NAME LABEL
        doctorMidView.addSubview(doctorNameLabel)
        doctorNameLabel.topAnchor.constraint(equalTo: doctorMidView.topAnchor, constant: 5).isActive = true
        doctorNameLabel.leadingAnchor.constraint(equalTo: doctorMidView.leadingAnchor, constant: 20).isActive = true
        doctorNameLabel.trailingAnchor.constraint(equalTo: doctorMidView.trailingAnchor, constant: -20).isActive = true
        
        ///ADDING VENUE DESC LABEL
        doctorMidView.addSubview(doctorSpecialityLabel)
        doctorSpecialityLabel.topAnchor.constraint(equalTo: doctorNameLabel.bottomAnchor).isActive = true
        doctorSpecialityLabel.leadingAnchor.constraint(equalTo: doctorMidView.leadingAnchor, constant: 20).isActive = true
        doctorSpecialityLabel.trailingAnchor.constraint(equalTo: doctorMidView.trailingAnchor, constant: -20).isActive = true
        
        ///ADDING SEPARATOR LINE
        doctorMidView.addSubview(doctorMidViewSeparatorLabel)
        doctorMidViewSeparatorLabel.topAnchor.constraint(equalTo: doctorSpecialityLabel.bottomAnchor, constant: 5).isActive = true
        doctorMidViewSeparatorLabel.leadingAnchor.constraint(equalTo: doctorMidView.leadingAnchor, constant: 15).isActive = true
        doctorMidViewSeparatorLabel.trailingAnchor.constraint(equalTo: doctorMidView.trailingAnchor, constant: -15).isActive = true
        doctorMidViewSeparatorLabel.heightAnchor.constraint(equalToConstant: 1).isActive = true
    }
    
    private func configureDoctorFooterViewUI(){
        scrollView.addSubview(doctorFooterView)
        doctorFooterView.topAnchor.constraint(equalTo: doctorMidView.bottomAnchor).isActive = true
        doctorFooterView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        doctorFooterView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        doctorFooterView.heightAnchor.constraint(equalToConstant: 260).isActive = true
        
        //ADDING FACILITIES ICON
        doctorFooterView.addSubview(emailIcon)
        emailIcon.topAnchor.constraint(equalTo: doctorFooterView.topAnchor, constant: 5).isActive = true
        emailIcon.leadingAnchor.constraint(equalTo: doctorFooterView.leadingAnchor, constant: 20).isActive = true
        emailIcon.heightAnchor.constraint(equalToConstant: 45).isActive = true
        emailIcon.widthAnchor.constraint(equalToConstant: 45).isActive = true
        
        doctorFooterView.addSubview(doctorEmailLabel)
        doctorEmailLabel.topAnchor.constraint(equalTo: doctorFooterView.topAnchor, constant: 15).isActive = true
        doctorEmailLabel.leadingAnchor.constraint(equalTo: emailIcon.trailingAnchor, constant: 10).isActive = true
        doctorEmailLabel.trailingAnchor.constraint(equalTo: doctorFooterView.trailingAnchor, constant: -15).isActive = true
        
        
        //ADDING TIMINGS ICON
        doctorFooterView.addSubview(timingsIcon)
        timingsIcon.topAnchor.constraint(equalTo: emailIcon.bottomAnchor, constant: 15).isActive = true
        timingsIcon.leadingAnchor.constraint(equalTo: doctorFooterView.leadingAnchor, constant: 20).isActive = true
        timingsIcon.heightAnchor.constraint(equalToConstant: 45).isActive = true
        timingsIcon.widthAnchor.constraint(equalToConstant: 45).isActive = true
        
        doctorFooterView.addSubview(timingsLabel)
        timingsLabel.topAnchor.constraint(equalTo: emailIcon.bottomAnchor, constant: 25).isActive = true
        timingsLabel.leadingAnchor.constraint(equalTo: timingsIcon.trailingAnchor, constant: 10).isActive = true
        timingsLabel.trailingAnchor.constraint(equalTo: doctorFooterView.trailingAnchor, constant: -15).isActive = true
        
        
        //ADDING CHARGES ICON
        doctorFooterView.addSubview(chargesIcon)
        chargesIcon.topAnchor.constraint(equalTo: timingsIcon.bottomAnchor, constant: 15).isActive = true
        chargesIcon.leadingAnchor.constraint(equalTo: doctorFooterView.leadingAnchor, constant: 20).isActive = true
        chargesIcon.heightAnchor.constraint(equalToConstant: 45).isActive = true
        chargesIcon.widthAnchor.constraint(equalToConstant: 45).isActive = true
        
        doctorFooterView.addSubview(chargesLabel)
        chargesLabel.topAnchor.constraint(equalTo: timingsIcon.bottomAnchor, constant: 25).isActive = true
        chargesLabel.leadingAnchor.constraint(equalTo: chargesIcon.trailingAnchor, constant: 10).isActive = true
        chargesLabel.trailingAnchor.constraint(equalTo: doctorFooterView.trailingAnchor, constant: -15).isActive = true
        
        //ADDING CONTACT ICON
        doctorFooterView.addSubview(doctorContactIcon)
        doctorContactIcon.topAnchor.constraint(equalTo: chargesIcon.bottomAnchor, constant: 15).isActive = true
        doctorContactIcon.leadingAnchor.constraint(equalTo: doctorFooterView.leadingAnchor, constant: 20).isActive = true
        doctorContactIcon.heightAnchor.constraint(equalToConstant: 45).isActive = true
        doctorContactIcon.widthAnchor.constraint(equalToConstant: 45).isActive = true
        
        doctorFooterView.addSubview(doctorContactLabel)
        doctorContactLabel.topAnchor.constraint(equalTo: chargesIcon.bottomAnchor, constant: 25).isActive = true
        doctorContactLabel.leadingAnchor.constraint(equalTo: doctorContactIcon.trailingAnchor, constant: 10).isActive = true
        doctorContactLabel.trailingAnchor.constraint(equalTo: doctorFooterView.trailingAnchor, constant: -15).isActive = true
    }
    
    private func configureDoctorBookingSlotsViewUI(){
        scrollView.addSubview(appointmentBookingSlotView)
        appointmentBookingSlotView.topAnchor.constraint(equalTo: doctorFooterView.bottomAnchor).isActive = true
        appointmentBookingSlotView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        appointmentBookingSlotView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        appointmentBookingSlotView.heightAnchor.constraint(equalToConstant: 270).isActive = true
        appointmentBookingSlotView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
        
        //Add book slots label
        appointmentBookingSlotView.addSubview(bookYourSlotsLabel)
        bookYourSlotsLabel.topAnchor.constraint(equalTo: appointmentBookingSlotView.topAnchor, constant: 10).isActive = true
        bookYourSlotsLabel.leadingAnchor.constraint(equalTo: appointmentBookingSlotView.leadingAnchor, constant: 15).isActive = true
        bookYourSlotsLabel.trailingAnchor.constraint(equalTo: appointmentBookingSlotView.trailingAnchor, constant: -15).isActive = true
        bookYourSlotsLabel.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        //Add availability label
        appointmentBookingSlotView.addSubview(bookYourSlotsDescLabel)
        bookYourSlotsDescLabel.topAnchor.constraint(equalTo: bookYourSlotsLabel.bottomAnchor, constant: 1).isActive = true
        bookYourSlotsDescLabel.leadingAnchor.constraint(equalTo: appointmentBookingSlotView.leadingAnchor, constant: 15).isActive = true
        bookYourSlotsDescLabel.trailingAnchor.constraint(equalTo: appointmentBookingSlotView.trailingAnchor, constant: -15).isActive = true
        bookYourSlotsDescLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        //Add date picker
        appointmentBookingSlotView.addSubview(datePicker)
        
        datePicker.frame = CGRect(x: 15, y: 95, width: 200, height: 40)
        
        datePicker.timeZone = NSTimeZone.local
        datePicker.backgroundColor = .systemBackground
        
        datePicker.addTarget(self, action: #selector(datePickerValueChanged(_:)), for: .valueChanged)
        
        //Add availability desc label
        appointmentBookingSlotView.addSubview(checkAvailabilityLabel)
        checkAvailabilityLabel.topAnchor.constraint(equalTo: datePicker.bottomAnchor, constant: 1).isActive = true
        checkAvailabilityLabel.leadingAnchor.constraint(equalTo: appointmentBookingSlotView.leadingAnchor, constant: 15).isActive = true
        checkAvailabilityLabel.trailingAnchor.constraint(equalTo: appointmentBookingSlotView.trailingAnchor, constant: -15).isActive = true
        checkAvailabilityLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        //Add availability button
        appointmentBookingSlotView.addSubview(checkAvailabilityButton)
        checkAvailabilityButton.topAnchor.constraint(equalTo: checkAvailabilityLabel.bottomAnchor, constant: 5).isActive = true
        checkAvailabilityButton.leadingAnchor.constraint(equalTo: appointmentBookingSlotView.leadingAnchor, constant: 35).isActive = true
        checkAvailabilityButton.trailingAnchor.constraint(equalTo: appointmentBookingSlotView.trailingAnchor, constant: -35).isActive = true
        checkAvailabilityButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
        checkAvailabilityButton.layer.cornerRadius = 10
    }
    
    private func configureScreenLabels(){
        doctorNameLabel.text = doctorName
        doctorSpecialityLabel.text = doctorSpeciality
        doctorEmailLabel.text = doctorEmail
    }
    
    private func presentConfirmHudAndDismissScreen(){
        //Initialize a Hud view obj
        let hudView = HudView.hud(inView: self.view, animated: true)
        
        //Set hud view text
        hudView.text = NSLocalizedString("Created", comment: "Created")
        
        //Dismiss hud view timings
        hudView.afterDelay(0.8) {
            hudView.hide()
            self.dismiss(animated: true, completion: nil)
            
            //Trigger notification here
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.NOTIFICATIONS.DISMISS_DOCTORS_SCREEN), object: nil)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.NOTIFICATIONS.NEW_APPOINTMENT_CREATED), object: nil)
        }
    }
    
    //MARK:- objc Methods
    @objc func datePickerValueChanged(_ sender: UIDatePicker) {
        checkAvailabilityLabel.isHidden = false
        
//        let dateFormatter: DateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
//        let selectedDate: String = dateFormatter.string(from: sender.date)
//        print("Selected Value \(Int(datePicker.date.timeIntervalSince1970))")
    }
    
    @objc func didTapCheckAvailabilityButton() {
        self.doctorsModel.createAppointment(doctorId) { (isConfirmed) in
            if isConfirmed {
                DispatchQueue.main.async {
                    self.presentConfirmHudAndDismissScreen()
                }
            } else {
                print("Error creating appointment")
            }
        }
    }


}
