//
//  SearchCategoryViewController.swift
//  Appointy
//
//  Created by Faiq on 16/04/2021.
//

import UIKit

class SearchSpecializationViewController: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var specialization1Segmented: UISegmentedControl!
    @IBOutlet weak var specialization2Segmented: UISegmentedControl!
    @IBOutlet weak var specializationTableView: UITableView!
    
    //MARK:- Variables
    private var doctorsModel = Routes()
    private var filteredData = [Doc]()
    
    //MARK:- Constant
    let segmentValue = ["All","Dentist","Cardiologist","Nephrologist","Ophthalmologists","General Physician"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //disable segment 2 when loading
        specialization2Segmented.selectedSegmentIndex = UISegmentedControl.noSegment
        
        //register XIB
        self.specializationTableView.register(UINib(nibName: "SearchDrTableViewCell", bundle: nil), forCellReuseIdentifier: SearchDrTableViewCell.reuseIdentifier)
        self.specializationTableView.register(UINib(nibName: "NoDrFoundTableViewCell", bundle: nil), forCellReuseIdentifier: NoDrFoundTableViewCell.reuseIdentifier)
        
        doctorsModel.vc1 = self
        doctorsModel.getAllDoctors()
        
        //Notification when all doctors are fetched
        NotificationCenter.default.addObserver(self, selector: #selector(getFetchedDoctors), name: NSNotification.Name(rawValue: Constants.NOTIFICATIONS.DOCTORS_FETCHED), object: nil)
    }
    
    //MARK:- IBActions
    @IBAction func firstSegment(_ sender: UISegmentedControl) {
        specialization2Segmented.selectedSegmentIndex = UISegmentedControl.noSegment
        //print(segmentValue[specialization1Segmented.selectedSegmentIndex])
        self.searchDrBySpecialization(specialization: segmentValue[specialization1Segmented.selectedSegmentIndex])
        self.specializationTableView.reloadData()
    }
    
    @IBAction func secondSegment(_ sender: UISegmentedControl) {
        specialization1Segmented.selectedSegmentIndex = UISegmentedControl.noSegment
        //print(segmentValue[specialization2Segmented.selectedSegmentIndex + 4])
        self.searchDrBySpecialization(specialization: segmentValue[specialization2Segmented.selectedSegmentIndex + 4])
        self.specializationTableView.reloadData()
    }
    
    //MARK:- Helper Methods
    private func searchDrBySpecialization(specialization: String) {
        filteredData = []

        if specialization == "All" {
            filteredData = doctorsModel.arrDoctors
        } else if specialization == "Dentist" {
            // Go through every element
            for doctor in doctorsModel.arrDoctors {
                if doctor.speciality!.contains(specialization) {
                    filteredData.append(doctor)
                }
            }
        } else if specialization == "Cardiologist" {
            // Go through every element
            for doctor in doctorsModel.arrDoctors {
                if doctor.speciality!.contains(specialization) {
                    filteredData.append(doctor)
                }
            }
        }else if specialization == "General Physician" {
            // Go through every element
            for doctor in doctorsModel.arrDoctors {
                if doctor.speciality!.contains(specialization) {
                    filteredData.append(doctor)
                }
            }
        } else if specialization == "Ophthalmologists" {
            // Go through every element
            for doctor in doctorsModel.arrDoctors {
                if doctor.speciality!.contains(specialization) {
                    filteredData.append(doctor)
                }
            }
        } else if specialization == "Nephrologist" {
            // Go through every element
            for doctor in doctorsModel.arrDoctors {
                if doctor.speciality!.contains(specialization) {
                    filteredData.append(doctor)
                }
            }
        }
    }
    
    @objc private func getFetchedDoctors(){
        filteredData = doctorsModel.arrDoctors
    }

}

//MARK:- Table View DataSource & Delegates
extension SearchSpecializationViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if filteredData.count == 0 {
            return 1
        } else {
            return filteredData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if filteredData.count == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: NoDrFoundTableViewCell.reuseIdentifier) as! NoDrFoundTableViewCell
            cell.configureDescriptionLabelForNoResultsFound()
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: SearchDrTableViewCell.reuseIdentifier) as! SearchDrTableViewCell
            cell.doctorNameLabel.text = "Dr. " +  filteredData[indexPath.row].name!
            cell.doctorSpecializationLabel.text = filteredData[indexPath.row].speciality
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if filteredData.count == 0 {
            tableView.deselectRow(at: indexPath, animated: false)
        } else {
            tableView.deselectRow(at: indexPath, animated: false)
            print(filteredData[indexPath.row])
            
            let vc = DoctorDetailViewController()
            let dr = filteredData[indexPath.row]
            
            vc.doctorName = dr.name!
            vc.doctorSpeciality = dr.speciality!
            vc.doctorEmail = dr.email!
            vc.doctorId = dr._id!
            
            //present segue, Dr detail profile
            present(vc, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if filteredData.count == 0 {
            return 314
        } else {
            return 125
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
}
