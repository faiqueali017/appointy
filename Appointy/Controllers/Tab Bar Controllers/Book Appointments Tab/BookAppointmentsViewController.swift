//
//  BookAppointmentsViewController.swift
//  Appointy
//
//  Created by Faiq on 15/04/2021.
//

import UIKit

class BookAppointmentsViewController: UIViewController {
    
    ///SEARCH DR VIEW
    private let searchDrView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 10
        view.backgroundColor = UIColor(named: "Custom-Home-1")
        view.isUserInteractionEnabled = true
        view.dropShadow(shadowOpacity: 0.4, shadowRadius: 13, shadowColor: UIColor.black.cgColor)
        return view
    }()
    
    private let searchDrViewIcon: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.backgroundColor = .white
        image.image = UIImage(named: "search-dr-icon")
        image.layer.cornerRadius = 5
        return image
    }()
    
    private let searchDrLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.text = "Search Doctor"
        label.font = UIFont(name: "AvenirNext-Bold", size: 25)
        label.textColor = .white
        return label
    }()
    
    private let searchDrDescLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.text = "You can search Dr here by the name, here by the name"
        label.font = UIFont(name: "Avenir-Light", size: 16)
        label.textColor = .white
        label.numberOfLines = 0
        //label.backgroundColor = .systemPink
        return label
    }()
    
    
    ///SEARCH DR CATEGORY VIEW
    private let searchCategoryView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 10
        view.backgroundColor = UIColor(named: "Custom-Home-2")
        view.isUserInteractionEnabled = true
        view.dropShadow(shadowOpacity: 0.4, shadowRadius: 13, shadowColor: UIColor.black.cgColor)
        return view
    }()
    
    private let searchCategoryViewIcon: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.backgroundColor = .white
        image.image = UIImage(named: "specialization-icon")
        image.layer.cornerRadius = 5
        return image
    }()
    
    private let searchCategoryLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.text = "Specialization"
        label.font = UIFont(name: "AvenirNext-Bold", size: 25)
        label.textColor = .white
        return label
    }()
    
    private let searchCategoryDescLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.text = "You can search Dr here by the specialization here, here by the name"
        label.font = UIFont(name: "Avenir-Light", size: 16)
        label.textColor = .white
        label.numberOfLines = 0
        //label.backgroundColor = .systemPink
        return label
    }()
    
    
    ///SEARCH SYMPTOMS VIEW
    private let searchSymptomsView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 10
        view.backgroundColor = UIColor(named: "Custom-Home-3")
        view.isUserInteractionEnabled = true
        view.dropShadow(shadowOpacity: 0.4, shadowRadius: 13, shadowColor: UIColor.black.cgColor)
        return view
    }()
    
    private let searchSymptomsViewIcon: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.backgroundColor = .white
        image.image = UIImage(named: "search-symptoms")
        image.layer.cornerRadius = 5
        return image
    }()
    
    private let searchSymptomsLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.text = "Search Symptoms"
        label.font = UIFont(name: "AvenirNext-Bold", size: 25)
        label.textColor = .white
        return label
    }()
    
    private let searchSymptomsDescLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.text = "You can search Dr here by the symptoms, here by the name"
        label.font = UIFont(name: "Avenir-Light", size: 16)
        label.textColor = .white
        label.numberOfLines = 0
        //label.backgroundColor = .systemPink
        return label
    }()
    
    ///CALL ASSIST
    private let callAssistView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 10
        view.backgroundColor = UIColor(named: "Custom-Home-4")
        view.isUserInteractionEnabled = true
        view.dropShadow(shadowOpacity: 0.4, shadowRadius: 13, shadowColor: UIColor.black.cgColor)
        return view
    }()
    
    private let callAssistViewIcon: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.backgroundColor = .white
        image.image = UIImage(named: "call-assist-icon")
        image.layer.cornerRadius = 5
        return image
    }()
    
    private let callAssistLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.text = "Call Assist"
        label.font = UIFont(name: "AvenirNext-Bold", size: 25)
        label.textColor = .white
        return label
    }()
    
    private let callAssistDescLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.text = "You can search Dr here by the name, here by the name"
        label.font = UIFont(name: "Avenir-Light", size: 16)
        label.textColor = .white
        label.numberOfLines = 0
        //label.backgroundColor = .systemPink
        return label
    }()
    
    ///APPOINTMENTS STACK VIEW
    private let bookAppointmentsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 25
        stackView.distribution = .fillEqually
        return stackView
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        setupTapGesturesOfViews()
    }
    
    //MARK:- Helper Methods
    private func setupViews(){
        
        //setting stack view constraints
        view.addSubview(bookAppointmentsStackView)
        bookAppointmentsStackView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 10).isActive = true
        bookAppointmentsStackView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -10).isActive = true
        bookAppointmentsStackView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 30).isActive = true
        bookAppointmentsStackView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -30).isActive = true
        
        setupSubViews(stackView: bookAppointmentsStackView, view: searchDrView, imageIcon: searchDrViewIcon, titleLabel: searchDrLabel, descLabel: searchDrDescLabel)
        
        setupSubViews(stackView: bookAppointmentsStackView, view: searchCategoryView, imageIcon: searchCategoryViewIcon, titleLabel: searchCategoryLabel, descLabel: searchCategoryDescLabel)
        
        setupSubViews(stackView: bookAppointmentsStackView, view: searchSymptomsView, imageIcon: searchSymptomsViewIcon, titleLabel: searchSymptomsLabel, descLabel: searchSymptomsDescLabel)

        setupSubViews(stackView: bookAppointmentsStackView, view: callAssistView, imageIcon: callAssistViewIcon, titleLabel: callAssistLabel, descLabel: callAssistDescLabel)
    }
    
    private func setupSubViews(stackView: UIStackView ,view: UIView, imageIcon: UIImageView, titleLabel: UILabel, descLabel: UILabel) {
        //Adding view to stack view
        bookAppointmentsStackView.addArrangedSubview(view)
    
        //Add image icon
        view.addSubview(imageIcon)
        imageIcon.heightAnchor.constraint(equalToConstant: 70).isActive = true
        imageIcon.widthAnchor.constraint(equalToConstant: 70).isActive = true
        imageIcon.topAnchor.constraint(equalTo: view.topAnchor, constant: 12).isActive = true
        imageIcon.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10).isActive = true
        imageIcon.dropShadow(shadowOpacity: 0.3, shadowRadius: 10, shadowColor: UIColor.black.cgColor)
        
        //Add label
        view.addSubview(titleLabel)
        titleLabel.heightAnchor.constraint(equalToConstant: 25).isActive = true
        titleLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 15).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: imageIcon.trailingAnchor, constant: 15).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10).isActive = true
        
        //Add Desc label
        view.addSubview(descLabel)
        //descLabel.heightAnchor.constraint(equalToConstant: 25).isActive = true
        descLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor).isActive = true
        descLabel.leadingAnchor.constraint(equalTo: imageIcon.trailingAnchor, constant: 15).isActive = true
        descLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10).isActive = true
        descLabel.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
    }
    
    private func setupTapGesturesOfViews(){
        let searchDrViewTap = UITapGestureRecognizer(target: self, action: #selector(searchDrViewTap(_:)))
        searchDrView.addGestureRecognizer(searchDrViewTap)
        
        //searchCategoryView
        let searchCategoryViewTap = UITapGestureRecognizer(target: self, action: #selector(searchCategoryViewTap(_:)))
        searchCategoryView.addGestureRecognizer(searchCategoryViewTap)
        
        //searchSymptomsView
        let searchSymptomsViewTap = UITapGestureRecognizer(target: self, action: #selector(searchSymptomsViewTap(_:)))
        searchSymptomsView.addGestureRecognizer(searchSymptomsViewTap)
        
        
        //callAssistView
        let callAssistViewTap = UITapGestureRecognizer(target: self, action: #selector(callAssistViewTap(_:)))
        callAssistView.addGestureRecognizer(callAssistViewTap)
        
    }
    
    
    //MARK:- View Tap Gestures Methods
    @objc func searchDrViewTap(_ sender: UITapGestureRecognizer){
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let searchDrViewController = storyboard.instantiateViewController(identifier: Constants.STORYBOARD.SEARCH_DR_VIEW_CONTROLLER_ID) as! SearchDrViewController
        self.present(searchDrViewController, animated: true, completion: nil)
        //self.navigationController?.pushViewController(searchDrViewController, animated: true)
    }
    
    @objc func searchCategoryViewTap(_ sender: UITapGestureRecognizer){
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let searchCategoryViewController = storyboard.instantiateViewController(identifier: Constants.STORYBOARD.SEARCH_DR_SPECIALIZATION_CONTROLLER_ID) as! SearchSpecializationViewController
        self.present(searchCategoryViewController, animated: true, completion: nil)
        //self.navigationController?.pushViewController(searchCategoryViewController, animated: true)
    }
    
    @objc func searchSymptomsViewTap(_ sender: UITapGestureRecognizer){
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let searchSymptomsViewController = storyboard.instantiateViewController(identifier: Constants.STORYBOARD.SEARCH_SYMPTOMS_CONTROLLER_ID)
        self.present(searchSymptomsViewController, animated: true, completion: nil)
        //self.navigationController?.pushViewController(searchSymptomsViewController, animated: true)
    }
    
    @objc func callAssistViewTap(_ sender: UITapGestureRecognizer){
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let callAssistViewController = storyboard.instantiateViewController(identifier: Constants.STORYBOARD.CALL_ASSIST_CONTROLLER_ID)
        self.present(callAssistViewController, animated: true, completion: nil)
        //self.navigationController?.pushViewController(callAssistViewController, animated: true)
    }

}
