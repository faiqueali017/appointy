//
//  SearchSymptomsViewController.swift
//  Appointy
//
//  Created by Faiq on 16/04/2021.
//

import UIKit

class SearchSymptomsViewController: UIViewController {
    
    private let scrollView: UIScrollView = {
        let view = UIScrollView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.showsVerticalScrollIndicator = false
        view.backgroundColor = .systemBackground
        return view
    }()
    
    /// SYMPTOMS VIEW
    private let symptomsHeaderView: UIView = {
        let view  = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 13.0, *) {
            view.backgroundColor = UIColor(named: "Custom-Purple-1")
        } else {
            view.backgroundColor = .white
        }
        return view
    }()
    
    private let symptomsHeaderViewImage: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "Questioning Symptoms")
        image.translatesAutoresizingMaskIntoConstraints = false
        image.contentMode = .scaleAspectFit
        return image
    }()
    
    private let symptomsQuesView: UIView = {
        let view  = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 13.0, *) {
            view.backgroundColor = .systemBackground
        } else {
            view.backgroundColor = .white
        }
        return view
    }()
    
    private let symptomsFindMyDrView: UIView = {
        let view  = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 13.0, *) {
            view.backgroundColor = .systemBackground
        } else {
            view.backgroundColor = .white
        }
        return view
    }()
    
    /// QUESTION 1
    private let question1Label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textAlignment = .left
        label.font = UIFont(name: "AvenirNext-Regular", size: 20)
        label.text = "1. Are you having Fever/ Headache /Cough / Cold?"
        //label.backgroundColor = .yellow
        if #available(iOS 13.0, *) {
            label.textColor = .label
        } else {
            label.textColor = .black
        }
        return label
    }()
    
    private let ques1OptionBtn1: UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.isSelected = false
        let image = UIImage(named: "circle-not-filled")?.withRenderingMode(.alwaysTemplate)
        btn.setImage(image, for: .normal)
        btn.tintColor = UIColor(named: "White-Black")
        btn.addTarget(self, action: #selector(didTapQuestion1Btn1(_:)), for: .touchUpInside)
        return btn
    }()
    
    private let ques1OptionBtn2: UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.isSelected = false
        let image = UIImage(named: "circle-not-filled")?.withRenderingMode(.alwaysTemplate)
        btn.setImage(image, for: .normal)
        btn.tintColor = UIColor(named: "White-Black")
        btn.addTarget(self, action: #selector(didTapQuestion1Btn2(_:)), for: .touchUpInside)
        return btn
    }()
    
    private let ques1Option1Label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textAlignment = .left
        label.font = UIFont(name: "AvenirNext-Regular", size: 20)
        label.text = "Yes"
        //label.backgroundColor = .yellow
        if #available(iOS 13.0, *) {
            label.textColor = .label
        } else {
            label.textColor = .black
        }
        return label
    }()
    
    private let ques1Option2Label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textAlignment = .left
        label.font = UIFont(name: "AvenirNext-Regular", size: 20)
        label.text = "No"
        //label.backgroundColor = .yellow
        if #available(iOS 13.0, *) {
            label.textColor = .label
        } else {
            label.textColor = .black
        }
        return label
    }()
    
    //QUESTION 2
    private let question2Label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textAlignment = .left
        label.font = UIFont(name: "AvenirNext-Regular", size: 20)
        label.text = "2. Are you having Chest pain / Shortness of breath / Heartburn?"
        //label.backgroundColor = .yellow
        if #available(iOS 13.0, *) {
            label.textColor = .label
        } else {
            label.textColor = .black
        }
        return label
    }()
    
    private let ques2OptionBtn1: UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.isSelected = false
        let image = UIImage(named: "circle-not-filled")?.withRenderingMode(.alwaysTemplate)
        btn.setImage(image, for: .normal)
        btn.tintColor = UIColor(named: "White-Black")
        btn.addTarget(self, action: #selector(didTapQuestion2Btn1(_:)), for: .touchUpInside)
        return btn
    }()
    
    private let ques2OptionBtn2: UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.isSelected = false
        let image = UIImage(named: "circle-not-filled")?.withRenderingMode(.alwaysTemplate)
        btn.setImage(image, for: .normal)
        btn.tintColor = UIColor(named: "White-Black")
        btn.addTarget(self, action: #selector(didTapQuestion2Btn2(_:)), for: .touchUpInside)
        return btn
    }()
    
    private let ques2Option1Label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textAlignment = .left
        label.font = UIFont(name: "AvenirNext-Regular", size: 20)
        label.text = "Yes"
        //label.backgroundColor = .yellow
        if #available(iOS 13.0, *) {
            label.textColor = .label
        } else {
            label.textColor = .black
        }
        return label
    }()
    
    private let ques2Option2Label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textAlignment = .left
        label.font = UIFont(name: "AvenirNext-Regular", size: 20)
        label.text = "No"
        //label.backgroundColor = .yellow
        if #available(iOS 13.0, *) {
            label.textColor = .label
        } else {
            label.textColor = .black
        }
        return label
    }()
    
    
    ///QUESTION 3
    private let question3Label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textAlignment = .left
        label.font = UIFont(name: "AvenirNext-Regular", size: 20)
        label.text = "3. Are you having Vomiting / Stomach Pain / Diarrhea / Constipation?"
        //label.backgroundColor = .yellow
        if #available(iOS 13.0, *) {
            label.textColor = .label
        } else {
            label.textColor = .black
        }
        return label
    }()
    
    private let ques3OptionBtn1: UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.isSelected = false
        let image = UIImage(named: "circle-not-filled")?.withRenderingMode(.alwaysTemplate)
        btn.setImage(image, for: .normal)
        btn.tintColor = UIColor(named: "White-Black")
        btn.addTarget(self, action: #selector(didTapQuestion3Btn1(_:)), for: .touchUpInside)
        return btn
    }()
    
    private let ques3OptionBtn2: UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.isSelected = false
        let image = UIImage(named: "circle-not-filled")?.withRenderingMode(.alwaysTemplate)
        btn.setImage(image, for: .normal)
        btn.tintColor = UIColor(named: "White-Black")
        btn.addTarget(self, action: #selector(didTapQuestion3Btn2(_:)), for: .touchUpInside)
        return btn
    }()
    
    private let ques3Option1Label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textAlignment = .left
        label.font = UIFont(name: "AvenirNext-Regular", size: 20)
        label.text = "Yes"
        //label.backgroundColor = .yellow
        if #available(iOS 13.0, *) {
            label.textColor = .label
        } else {
            label.textColor = .black
        }
        return label
    }()
    
    private let ques3Option2Label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textAlignment = .left
        label.font = UIFont(name: "AvenirNext-Regular", size: 20)
        label.text = "No"
        //label.backgroundColor = .yellow
        if #available(iOS 13.0, *) {
            label.textColor = .label
        } else {
            label.textColor = .black
        }
        return label
    }()

    //QUESTION 4
    private let question4Label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textAlignment = .left
        label.font = UIFont(name: "AvenirNext-Regular", size: 20)
        label.text = "4. Are you having Diabetes / Cholesterol / Hypertension?"
        //label.backgroundColor = .yellow
        if #available(iOS 13.0, *) {
            label.textColor = .label
        } else {
            label.textColor = .black
        }
        return label
    }()
    
    private let ques4OptionBtn1: UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.isSelected = false
        let image = UIImage(named: "circle-not-filled")?.withRenderingMode(.alwaysTemplate)
        btn.setImage(image, for: .normal)
        btn.tintColor = UIColor(named: "White-Black")
        btn.addTarget(self, action: #selector(didTapQuestion4Btn1(_:)), for: .touchUpInside)
        return btn
    }()
    
    private let ques4OptionBtn2: UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.isSelected = false
        let image = UIImage(named: "circle-not-filled")?.withRenderingMode(.alwaysTemplate)
        btn.setImage(image, for: .normal)
        btn.tintColor = UIColor(named: "White-Black")
        btn.addTarget(self, action: #selector(didTapQuestion4Btn2(_:)), for: .touchUpInside)
        return btn
    }()
    
    private let ques4Option1Label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textAlignment = .left
        label.font = UIFont(name: "AvenirNext-Regular", size: 20)
        label.text = "Yes"
        //label.backgroundColor = .yellow
        if #available(iOS 13.0, *) {
            label.textColor = .label
        } else {
            label.textColor = .black
        }
        return label
    }()
    
    private let ques4Option2Label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textAlignment = .left
        label.font = UIFont(name: "AvenirNext-Regular", size: 20)
        label.text = "No"
        //label.backgroundColor = .yellow
        if #available(iOS 13.0, *) {
            label.textColor = .label
        } else {
            label.textColor = .black
        }
        return label
    }()
    
    ///FIND MY DR BUTTON
    private let findMyDrButton: UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.isSelected = false
        btn.setTitle("Find My Doctor", for: .normal)
        btn.titleLabel?.font = UIFont(name: "Avenir Next", size: 22)
        btn.backgroundColor = UIColor(named: "Custom-Btn-1")
        btn.layer.cornerRadius = 10
        btn.addTarget(self, action: #selector(didTapFindMyDrBtn(_:)), for: .touchUpInside)
        return btn
    }()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
        self.navigationItem.title = "Search Symptoms"
    }
    
    //MARK:- Helper Methods
    private func setupUI(){
        //adding scroll view as a main
        view.addSubview(scrollView)
        scrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        
        ///-----------------SYMPTOMS VIEW---------------------
        scrollView.addSubview(symptomsHeaderView)
        symptomsHeaderView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        symptomsHeaderView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        symptomsHeaderView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        symptomsHeaderView.heightAnchor.constraint(equalToConstant: 250).isActive = true
        
        //Add image view
        symptomsHeaderView.addSubview(symptomsHeaderViewImage)
        symptomsHeaderViewImage.topAnchor.constraint(equalTo: symptomsHeaderView.topAnchor).isActive = true
        symptomsHeaderViewImage.leadingAnchor.constraint(equalTo: symptomsHeaderView.leadingAnchor).isActive = true
        symptomsHeaderViewImage.trailingAnchor.constraint(equalTo: symptomsHeaderView.trailingAnchor).isActive = true
        symptomsHeaderViewImage.bottomAnchor.constraint(equalTo: symptomsHeaderView.bottomAnchor).isActive = true
        
        
        scrollView.addSubview(symptomsQuesView)
        symptomsQuesView.topAnchor.constraint(equalTo: symptomsHeaderView.bottomAnchor).isActive = true
        symptomsQuesView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        symptomsQuesView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        symptomsQuesView.heightAnchor.constraint(equalToConstant: 500).isActive = true
        
        scrollView.addSubview(symptomsFindMyDrView)
        symptomsFindMyDrView.topAnchor.constraint(equalTo: symptomsQuesView.bottomAnchor).isActive = true
        symptomsFindMyDrView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        symptomsFindMyDrView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        symptomsFindMyDrView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        symptomsFindMyDrView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
        
        /// QUESTIONS VIEW
        setupQuestions()
    }
    
    private func setupQuestions(){
        setupQuestion1()
        setupQuestion2()
        setupQuestion3()
        setupQuestion4()
        setupFindMyDrButton()
    }
    
    private func setupQuestion1(){
        symptomsQuesView.addSubview(question1Label)
        question1Label.topAnchor.constraint(equalTo: symptomsQuesView.topAnchor, constant: 20).isActive = true
        question1Label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        question1Label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        question1Label.heightAnchor.constraint(equalToConstant: 60).isActive = true
        //60 for 2 lines
        //80 for 3 lines
        symptomsQuesView.addSubview(ques1OptionBtn1)
        ques1OptionBtn1.topAnchor.constraint(equalTo: question1Label.bottomAnchor, constant: 8).isActive = true
        ques1OptionBtn1.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 40).isActive = true
        ques1OptionBtn1.heightAnchor.constraint(equalToConstant: 30).isActive = true
        ques1OptionBtn1.widthAnchor.constraint(equalToConstant: 30).isActive = true
        
        symptomsQuesView.addSubview(ques1Option1Label)
        ques1Option1Label.topAnchor.constraint(equalTo: question1Label.bottomAnchor, constant: 8).isActive = true
        ques1Option1Label.leadingAnchor.constraint(equalTo: ques1OptionBtn1.trailingAnchor, constant: 10).isActive = true
        ques1Option1Label.heightAnchor.constraint(equalToConstant: 30).isActive = true
        ques1Option1Label.widthAnchor.constraint(equalToConstant: 45).isActive = true
        
        symptomsQuesView.addSubview(ques1OptionBtn2)
        ques1OptionBtn2.topAnchor.constraint(equalTo: question1Label.bottomAnchor, constant: 8).isActive = true
        ques1OptionBtn2.leadingAnchor.constraint(equalTo: ques1Option1Label.trailingAnchor, constant: 25).isActive = true
        ques1OptionBtn2.heightAnchor.constraint(equalToConstant: 30).isActive = true
        ques1OptionBtn2.widthAnchor.constraint(equalToConstant: 30).isActive = true
        
        symptomsQuesView.addSubview(ques1Option2Label)
        ques1Option2Label.topAnchor.constraint(equalTo: question1Label.bottomAnchor, constant: 8).isActive = true
        ques1Option2Label.leadingAnchor.constraint(equalTo: ques1OptionBtn2.trailingAnchor, constant: 10).isActive = true
        ques1Option2Label.heightAnchor.constraint(equalToConstant: 30).isActive = true
        ques1Option2Label.widthAnchor.constraint(equalToConstant: 45).isActive = true
    }
    
    private func setupQuestion2(){
        symptomsQuesView.addSubview(question2Label)
        question2Label.topAnchor.constraint(equalTo: ques1OptionBtn1.bottomAnchor, constant: 20).isActive = true
        question2Label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        question2Label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        question2Label.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        symptomsQuesView.addSubview(ques2OptionBtn1)
        ques2OptionBtn1.topAnchor.constraint(equalTo: question2Label.bottomAnchor, constant: 8).isActive = true
        ques2OptionBtn1.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 40).isActive = true
        ques2OptionBtn1.heightAnchor.constraint(equalToConstant: 30).isActive = true
        ques2OptionBtn1.widthAnchor.constraint(equalToConstant: 30).isActive = true
        
        symptomsQuesView.addSubview(ques2Option1Label)
        ques2Option1Label.topAnchor.constraint(equalTo: question2Label.bottomAnchor, constant: 8).isActive = true
        ques2Option1Label.leadingAnchor.constraint(equalTo: ques2OptionBtn1.trailingAnchor, constant: 10).isActive = true
        ques2Option1Label.heightAnchor.constraint(equalToConstant: 30).isActive = true
        ques2Option1Label.widthAnchor.constraint(equalToConstant: 45).isActive = true
        
        symptomsQuesView.addSubview(ques2OptionBtn2)
        ques2OptionBtn2.topAnchor.constraint(equalTo: question2Label.bottomAnchor, constant: 8).isActive = true
        ques2OptionBtn2.leadingAnchor.constraint(equalTo: ques2Option1Label.trailingAnchor, constant: 25).isActive = true
        ques2OptionBtn2.heightAnchor.constraint(equalToConstant: 30).isActive = true
        ques2OptionBtn2.widthAnchor.constraint(equalToConstant: 30).isActive = true
        
        symptomsQuesView.addSubview(ques2Option2Label)
        ques2Option2Label.topAnchor.constraint(equalTo: question2Label.bottomAnchor, constant: 8).isActive = true
        ques2Option2Label.leadingAnchor.constraint(equalTo: ques2OptionBtn2.trailingAnchor, constant: 10).isActive = true
        ques2Option2Label.heightAnchor.constraint(equalToConstant: 30).isActive = true
        ques2Option2Label.widthAnchor.constraint(equalToConstant: 45).isActive = true
    }
    
    private func setupQuestion3(){
        symptomsQuesView.addSubview(question3Label)
        question3Label.topAnchor.constraint(equalTo: ques2OptionBtn1.bottomAnchor, constant: 20).isActive = true
        question3Label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        question3Label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        question3Label.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        symptomsQuesView.addSubview(ques3OptionBtn1)
        ques3OptionBtn1.topAnchor.constraint(equalTo: question3Label.bottomAnchor, constant: 8).isActive = true
        ques3OptionBtn1.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 40).isActive = true
        ques3OptionBtn1.heightAnchor.constraint(equalToConstant: 30).isActive = true
        ques3OptionBtn1.widthAnchor.constraint(equalToConstant: 30).isActive = true
        
        symptomsQuesView.addSubview(ques3Option1Label)
        ques3Option1Label.topAnchor.constraint(equalTo: question3Label.bottomAnchor, constant: 8).isActive = true
        ques3Option1Label.leadingAnchor.constraint(equalTo: ques3OptionBtn1.trailingAnchor, constant: 10).isActive = true
        ques3Option1Label.heightAnchor.constraint(equalToConstant: 30).isActive = true
        ques3Option1Label.widthAnchor.constraint(equalToConstant: 45).isActive = true
        
        symptomsQuesView.addSubview(ques3OptionBtn2)
        ques3OptionBtn2.topAnchor.constraint(equalTo: question3Label.bottomAnchor, constant: 8).isActive = true
        ques3OptionBtn2.leadingAnchor.constraint(equalTo: ques3Option1Label.trailingAnchor, constant: 25).isActive = true
        ques3OptionBtn2.heightAnchor.constraint(equalToConstant: 30).isActive = true
        ques3OptionBtn2.widthAnchor.constraint(equalToConstant: 30).isActive = true
        
        symptomsQuesView.addSubview(ques3Option2Label)
        ques3Option2Label.topAnchor.constraint(equalTo: question3Label.bottomAnchor, constant: 8).isActive = true
        ques3Option2Label.leadingAnchor.constraint(equalTo: ques3OptionBtn2.trailingAnchor, constant: 10).isActive = true
        ques3Option2Label.heightAnchor.constraint(equalToConstant: 30).isActive = true
        ques3Option2Label.widthAnchor.constraint(equalToConstant: 45).isActive = true
    }
    
    private func setupQuestion4(){
        symptomsQuesView.addSubview(question4Label)
        question4Label.topAnchor.constraint(equalTo: ques3OptionBtn1.bottomAnchor, constant: 20).isActive = true
        question4Label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        question4Label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        question4Label.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        symptomsQuesView.addSubview(ques4OptionBtn1)
        ques4OptionBtn1.topAnchor.constraint(equalTo: question4Label.bottomAnchor, constant: 8).isActive = true
        ques4OptionBtn1.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 40).isActive = true
        ques4OptionBtn1.heightAnchor.constraint(equalToConstant: 30).isActive = true
        ques4OptionBtn1.widthAnchor.constraint(equalToConstant: 30).isActive = true
        
        symptomsQuesView.addSubview(ques4Option1Label)
        ques4Option1Label.topAnchor.constraint(equalTo: question4Label.bottomAnchor, constant: 8).isActive = true
        ques4Option1Label.leadingAnchor.constraint(equalTo: ques4OptionBtn1.trailingAnchor, constant: 10).isActive = true
        ques4Option1Label.heightAnchor.constraint(equalToConstant: 30).isActive = true
        ques4Option1Label.widthAnchor.constraint(equalToConstant: 45).isActive = true
        
        symptomsQuesView.addSubview(ques4OptionBtn2)
        ques4OptionBtn2.topAnchor.constraint(equalTo: question4Label.bottomAnchor, constant: 8).isActive = true
        ques4OptionBtn2.leadingAnchor.constraint(equalTo: ques4Option1Label.trailingAnchor, constant: 25).isActive = true
        ques4OptionBtn2.heightAnchor.constraint(equalToConstant: 30).isActive = true
        ques4OptionBtn2.widthAnchor.constraint(equalToConstant: 30).isActive = true
        
        symptomsQuesView.addSubview(ques4Option2Label)
        ques4Option2Label.topAnchor.constraint(equalTo: question4Label.bottomAnchor, constant: 8).isActive = true
        ques4Option2Label.leadingAnchor.constraint(equalTo: ques4OptionBtn2.trailingAnchor, constant: 10).isActive = true
        ques4Option2Label.heightAnchor.constraint(equalToConstant: 30).isActive = true
        ques4Option2Label.widthAnchor.constraint(equalToConstant: 45).isActive = true
    }
    
    private func setupFindMyDrButton() {
        symptomsFindMyDrView.addSubview(findMyDrButton)
        findMyDrButton.topAnchor.constraint(equalTo: symptomsFindMyDrView.topAnchor, constant: 10).isActive = true
        findMyDrButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        findMyDrButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        //findMyDrButton.bottomAnchor.constraint(equalTo: symptomsFindMyDrView.bottomAnchor, constant: -10).isActive = true
        findMyDrButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
    }
    
    private func setCircleNotFilled(_ sender: UIButton){
        sender.isSelected = false
        sender.setImage(UIImage(named: "circle-not-filled")?.withRenderingMode(.alwaysTemplate), for: .normal)
        sender.tintColor = UIColor(named: "White-Black")
    }
    
    private func setCircleFilled(_ sender: UIButton, _ optionBtn: UIButton) {
        optionBtn.isSelected = false
        optionBtn.setImage(UIImage(named: "circle-not-filled")?.withRenderingMode(.alwaysTemplate), for: .normal)
        optionBtn.tintColor = UIColor(named: "White-Black")
        
        sender.isSelected = true
        sender.setImage(UIImage(named: "checkmark-icon")?.withRenderingMode(.alwaysTemplate), for: .normal)
        sender.tintColor = UIColor(named: "Custom-Btn-2")
    }
    
    //MARK:- objc Buttons methods
    @objc func didTapQuestion1Btn1(_ sender: UIButton) {
        if sender.isSelected {
            setCircleNotFilled(sender)
        } else {
            setCircleFilled(sender, ques1OptionBtn2)
        }
    }
    
    @objc func didTapQuestion1Btn2(_ sender: UIButton) {
        if sender.isSelected {
            setCircleNotFilled(sender)
        } else {
            setCircleFilled(sender, ques1OptionBtn1)
        }
    }
    
    @objc func didTapQuestion2Btn1(_ sender: UIButton) {
        if sender.isSelected {
            setCircleNotFilled(sender)
        } else {
            setCircleFilled(sender, ques2OptionBtn2)
        }
    }
    
    @objc func didTapQuestion2Btn2(_ sender: UIButton) {
        if sender.isSelected {
            setCircleNotFilled(sender)
        } else {
            setCircleFilled(sender, ques2OptionBtn1)
        }
    }
    
    @objc func didTapQuestion3Btn1(_ sender: UIButton) {
        if sender.isSelected {
            setCircleNotFilled(sender)
        } else {
            setCircleFilled(sender, ques3OptionBtn2)
        }
    }
    
    @objc func didTapQuestion3Btn2(_ sender: UIButton) {
        if sender.isSelected {
            setCircleNotFilled(sender)
        } else {
            setCircleFilled(sender, ques3OptionBtn1)
        }
    }
    
    @objc func didTapQuestion4Btn1(_ sender: UIButton) {
        if sender.isSelected {
            setCircleNotFilled(sender)
        } else {
           setCircleFilled(sender, ques4OptionBtn2)
        }
    }
    
    @objc func didTapQuestion4Btn2(_ sender: UIButton) {
        if sender.isSelected {
            setCircleNotFilled(sender)
        } else {
            setCircleFilled(sender, ques4OptionBtn1)
        }
    }
    
    @objc func didTapFindMyDrBtn(_ sender: UIButton) {
        FindMyDrViewController.speciality = findDoctorSpecialization()
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let findMyDrViewController = storyboard.instantiateViewController(identifier: Constants.STORYBOARD.FIND_MY_DR_CONTROLLER_ID)
        //self.present(searchSymptomsViewController, animated: true, completion: nil)
        self.navigationController?.pushViewController(findMyDrViewController, animated: true)
    }
    
    private func findDoctorSpecialization() -> String {
        ///ONE-TO-ONE-CASES
        if ques1OptionBtn1.isSelected == true && ques2OptionBtn1.isSelected == false && ques3OptionBtn1.isSelected == false && ques4OptionBtn1.isSelected == false {
            return "General Physician"
        }
        else if ques1OptionBtn1.isSelected == false && ques2OptionBtn1.isSelected == true && ques3OptionBtn1.isSelected == false && ques4OptionBtn1.isSelected == false {
            return "Cardiologist"
        }
        else if ques1OptionBtn1.isSelected == false && ques2OptionBtn1.isSelected == false && ques3OptionBtn1.isSelected == true && ques4OptionBtn1.isSelected == false {
            return "Gastroenterologist"
        } else if ques1OptionBtn1.isSelected == false && ques2OptionBtn1.isSelected == false && ques3OptionBtn1.isSelected == false && ques4OptionBtn1.isSelected == true {
            return "General Physician"
        }
        
        ///MEDIUM CASES
        else if ques1OptionBtn1.isSelected == true && ques2OptionBtn1.isSelected == true && ques3OptionBtn1.isSelected == false && ques4OptionBtn1.isSelected == false {
            return "General Physician"
        } else if ques1OptionBtn1.isSelected == true && ques2OptionBtn1.isSelected == false && ques3OptionBtn1.isSelected == false && ques4OptionBtn1.isSelected == true {
            return "General Physician"
        } else if ques1OptionBtn1.isSelected == false && ques2OptionBtn1.isSelected == true && ques3OptionBtn1.isSelected == true && ques4OptionBtn1.isSelected == false {
            return "General Physician"
        } else if ques1OptionBtn1.isSelected == false && ques2OptionBtn1.isSelected == false && ques3OptionBtn1.isSelected == false && ques4OptionBtn1.isSelected == false {
            return "General Physician"
        } else if ques1OptionBtn1.isSelected == true && ques2OptionBtn1.isSelected == false && ques3OptionBtn1.isSelected == true && ques4OptionBtn1.isSelected == false {
            return "General Physician"
        }
            
        ///WORST CASES
        else if ques1OptionBtn1.isSelected == true && ques2OptionBtn1.isSelected == true && ques3OptionBtn1.isSelected == true && ques4OptionBtn1.isSelected == true {
            return "General Physician"
        } else if ques1OptionBtn1.isSelected == true && ques2OptionBtn1.isSelected == true && ques3OptionBtn1.isSelected == true && ques4OptionBtn1.isSelected == false {
            return "General Physician"
        } else if ques1OptionBtn1.isSelected == true && ques2OptionBtn1.isSelected == true && ques3OptionBtn1.isSelected == false && ques4OptionBtn1.isSelected == true {
            return "General Physician"
        } else if ques1OptionBtn1.isSelected == true && ques2OptionBtn1.isSelected == false && ques3OptionBtn1.isSelected == true && ques4OptionBtn1.isSelected == true {
            return "General Physician"
        } else if ques1OptionBtn1.isSelected == false && ques2OptionBtn1.isSelected == true && ques3OptionBtn1.isSelected == true && ques4OptionBtn1.isSelected == true {
            return "General Physician"
        } else {
            return "General Physician"
        }
    }
}
