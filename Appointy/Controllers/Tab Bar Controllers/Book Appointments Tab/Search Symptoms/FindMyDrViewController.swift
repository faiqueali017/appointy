//
//  FindMyDrViewController.swift
//  Appointy
//
//  Created by Faiq on 19/04/2021.
//

import UIKit

class FindMyDrViewController: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var specialistNameLabel: UILabel!
    @IBOutlet weak var middleBreakerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var specialityLabel: UILabel!
    
    //MARK:- Variables
    private var doctorsModel = Routes()
    private var filteredData = [Doc]()
    static var speciality = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureScreen()
    }
    
    //MARK:- Helper Methods
    private func configureScreen(){
        self.navigationItem.title = "Find My Doctor"
        specialityLabel.text = "\(FindMyDrViewController.speciality)"
        
        //register XIB
        self.tableView.register(UINib(nibName: "SearchDrTableViewCell", bundle: nil), forCellReuseIdentifier: SearchDrTableViewCell.reuseIdentifier)
        self.tableView.register(UINib(nibName: "NoDrFoundTableViewCell", bundle: nil), forCellReuseIdentifier: NoDrFoundTableViewCell.reuseIdentifier)
        
        doctorsModel.vc2 = self
        doctorsModel.getAllDoctors()
        
        //Notification when all doctors are fetched
        NotificationCenter.default.addObserver(self, selector: #selector(getFetchedDoctors), name: NSNotification.Name(rawValue: Constants.NOTIFICATIONS.DOCTORS_FETCHED), object: nil)
    }
    
    @objc private func getFetchedDoctors(){
        // Initialize empty filterData
        filteredData = []

        for doctor in doctorsModel.arrDoctors {
            if FindMyDrViewController.speciality == doctor.speciality {
                filteredData.append(doctor)
            }
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
       
    
}

//MARK:- Table View DataSource & Delegates
extension FindMyDrViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if filteredData.count == 0 {
            return 1
        } else {
            return filteredData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if filteredData.count == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: NoDrFoundTableViewCell.reuseIdentifier) as! NoDrFoundTableViewCell
            cell.configureDescriptionLabelForNoResultsFound()
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: SearchDrTableViewCell.reuseIdentifier) as! SearchDrTableViewCell
            cell.doctorNameLabel.text = "Dr. " +  filteredData[indexPath.row].name!
            cell.doctorSpecializationLabel.text = filteredData[indexPath.row].speciality
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if filteredData.count == 0 {
            tableView.deselectRow(at: indexPath, animated: false)
        } else {
            tableView.deselectRow(at: indexPath, animated: false)
            print(filteredData[indexPath.row])
            
            let vc = DoctorDetailViewController()
            let dr = filteredData[indexPath.row]
            
            vc.doctorName = dr.name!
            vc.doctorSpeciality = dr.speciality!
            vc.doctorEmail = dr.email!
            vc.doctorId = dr._id!
            
            //present segue, Dr detail profile
            present(vc, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if filteredData.count == 0 {
            return 314
        } else {
            return 125
        }
    }
    

}
