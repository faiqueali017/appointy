//
//  SearchDrViewController.swift
//  Appointy
//
//  Created by Faiq on 16/04/2021.
//

import UIKit

class SearchDrViewController: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var drSearchBar: UISearchBar!
    @IBOutlet weak var drTableView: UITableView!
    
    //MARK:- Variables
    private var doctorsModel = Routes()
    private var filteredData = [Doc]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureSearchDrViewScreen()
    }
    
    //MARK:- Helper Methods
    private func configureSearchDrViewScreen(){
        //tableview and search bar delegates set in storyboard
       
        doctorsModel.vc = self
        doctorsModel.getAllDoctors()
        
        //register XIB
        self.drTableView.register(UINib(nibName: "SearchDrTableViewCell", bundle: nil), forCellReuseIdentifier: SearchDrTableViewCell.reuseIdentifier)
        self.drTableView.register(UINib(nibName: "NoDrFoundTableViewCell", bundle: nil), forCellReuseIdentifier: NoDrFoundTableViewCell.reuseIdentifier)
        
        //Notification when all doctors are fetched
        NotificationCenter.default.addObserver(self, selector: #selector(getFetchedDoctors), name: NSNotification.Name(rawValue: Constants.NOTIFICATIONS.DOCTORS_FETCHED), object: nil)
        
        //Notification when appointment is created to dismiss this screen
        NotificationCenter.default.addObserver(self, selector: #selector(dismissViewController), name: NSNotification.Name(rawValue: Constants.NOTIFICATIONS.DISMISS_DOCTORS_SCREEN), object: nil)
    }
    
    @objc private func getFetchedDoctors(){
        filteredData = doctorsModel.arrDoctors
    }
    
    @objc private func dismissViewController(){
        dismiss(animated: true, completion: nil)
    }

}

//MARK:- Table View DataSource & Delegates
extension SearchDrViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if filteredData.count == 0 {
            return 1
        } else {
            return filteredData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if filteredData.count == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: NoDrFoundTableViewCell.reuseIdentifier) as! NoDrFoundTableViewCell
            cell.configureDescriptionLabelForNoResultsFound()
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: SearchDrTableViewCell.reuseIdentifier) as! SearchDrTableViewCell
            cell.doctorNameLabel.text = "Dr. " +  filteredData[indexPath.row].name!
            cell.doctorSpecializationLabel.text = filteredData[indexPath.row].speciality
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if filteredData.count == 0 {
            tableView.deselectRow(at: indexPath, animated: false)
        } else {
            tableView.deselectRow(at: indexPath, animated: false)
            print(filteredData[indexPath.row])
            
            let vc = DoctorDetailViewController()
            let dr = filteredData[indexPath.row]
            
            vc.doctorName = dr.name!
            vc.doctorSpeciality = dr.speciality!
            vc.doctorEmail = dr.email!
            vc.doctorId = dr._id!
            
            //present segue, Dr detail profile
            present(vc, animated: true, completion: nil)
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if filteredData.count == 0 {
            return 314
        } else {
            return 125
        }
    }

}

//MARK:- Searchbar Delegates
extension SearchDrViewController: UISearchBarDelegate {
    
    //Will trigger everytime, when search bar text will change
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // Initialize empty filterData
        filteredData = []

        if searchText == "" {
            filteredData = doctorsModel.arrDoctors
        } else {
            // Go through every element
            for doctor in doctorsModel.arrDoctors {
                if
                    doctor.name!.lowercased().contains(searchText.lowercased()) {
                    filteredData.append(doctor)
                }
            }
        }
        self.drTableView.reloadData()

    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    
}
