//
//  CallAssistViewController.swift
//  Appointy
//
//  Created by Faiq on 16/04/2021.
//

import UIKit

class CallAssistViewController: UIViewController {
    
    private let callAssistBgView: UIView = {
        let view  = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 13.0, *) {
            view.backgroundColor = UIColor(named: "Custom-Indigo-1")
        } else {
            view.backgroundColor = .white
        }
        view.layer.cornerRadius = 10
        view.dropShadow(shadowOpacity: 0.3, shadowRadius: 12, shadowColor: UIColor(named: "White-Black")!.cgColor)
        return view
    }()
    
    private let callAssistTitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textAlignment = .center
        label.font = UIFont(name: "AvenirNext-Bold", size: 30)
        label.text = "Need any help?"
        if #available(iOS 13.0, *) {
            label.textColor = UIColor(named: "Custom-Purple-2")
        } else {
            label.textColor = .black
        }
        return label
    }()
    
    private let callAssistDescLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textAlignment = .center
        label.font = UIFont(name: "AvenirNext-Regular", size: 20)
        label.text = "You can call us for any query. We are always available for you."
        if #available(iOS 13.0, *) {
            label.textColor = UIColor(named: "Custom-Purple-2")
        } else {
            label.textColor = .black
        }
        return label
    }()
    
    private let callBtn: UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setImage(UIImage(named: "callReceiver"), for: .normal)
        btn.backgroundColor = UIColor(named: "Custom-Btn-3")
        //btn.imageView?.tintColor = .white
        btn.addTarget(self, action: #selector(didTapCallBtn(_:)), for: .touchUpInside)
        btn.dropShadow(shadowOpacity: 0.3, shadowRadius: 10, shadowColor: UIColor.black.cgColor)
        return btn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor(named: "Custom-Purple-1")
        self.navigationItem.title = "Call Assist"
        setupUI()
    }
    
    //MARK:- Helper Methods
    private func setupUI(){
        view.addSubview(callAssistBgView)
        
        //Add background view
        callAssistBgView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        callAssistBgView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        callAssistBgView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 50).isActive = true
        callAssistBgView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -50).isActive = true
        callAssistBgView.heightAnchor.constraint(equalToConstant: 300).isActive = true
        
        //Add Title label
        callAssistBgView.addSubview(callAssistTitleLabel)
        callAssistTitleLabel.topAnchor.constraint(equalTo: callAssistBgView.topAnchor, constant: 20).isActive = true
        callAssistTitleLabel.leadingAnchor.constraint(equalTo: callAssistBgView.leadingAnchor).isActive = true
        callAssistTitleLabel.trailingAnchor.constraint(equalTo: callAssistBgView.trailingAnchor).isActive = true
        
        //Add Desc label
        callAssistBgView.addSubview(callAssistDescLabel)
        callAssistDescLabel.topAnchor.constraint(equalTo: callAssistTitleLabel.bottomAnchor, constant: 10).isActive = true
        callAssistDescLabel.leadingAnchor.constraint(equalTo: callAssistBgView.leadingAnchor, constant: 10).isActive = true
        callAssistDescLabel.trailingAnchor.constraint(equalTo: callAssistBgView.trailingAnchor, constant: -10).isActive = true
        
        //Add Call Btn
        callAssistBgView.addSubview(callBtn)
        callBtn.bottomAnchor.constraint(equalTo: callAssistBgView.bottomAnchor, constant: -20).isActive = true
        callBtn.widthAnchor.constraint(equalToConstant: 70).isActive = true
        callBtn.heightAnchor.constraint(equalToConstant: 70).isActive = true
        callBtn.centerXAnchor.constraint(equalTo: callAssistBgView.centerXAnchor).isActive = true
        callBtn.layer.cornerRadius = 70/2
        
    }
    
    @objc func didTapCallBtn(_ sender: UIButton) {
        print("Call btn pressed")
    }
    
}
