//
//  MyAppointmentsViewController.swift
//  Appointy
//
//  Created by Faiq on 15/05/2021.
//

import UIKit

class MyAppointmentsViewController: UIViewController {
    
    //MARK:- IBOutlets
    @IBOutlet weak var appointmentTableView: UITableView!
    
    //MARK:- Variables
    private var appointmentsModel = Routes()
    private var filteredData = [Appointments]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureMyAppointmentsViewScreen()
    }
    
    //MARK:- Helper Methods
    private func configureMyAppointmentsViewScreen(){
        //tableview and search bar delegates set in storyboard
       
        appointmentsModel.vc3 = self
        appointmentsModel.getAllAppointments()
        
        //register XIB
        self.appointmentTableView.register(UINib(nibName: "AppointmentTableViewCell", bundle: nil), forCellReuseIdentifier: AppointmentTableViewCell.reuseIdentifier)
        self.appointmentTableView.register(UINib(nibName: "NoDrFoundTableViewCell", bundle: nil), forCellReuseIdentifier: NoDrFoundTableViewCell.reuseIdentifier)
        
        //Notification when all appointments are fetched
        NotificationCenter.default.addObserver(self, selector: #selector(getFetchedAppointments), name: NSNotification.Name(rawValue: Constants.NOTIFICATIONS.APPOINTMENTS_FETCHED), object: nil)
        
        //Notification when all appointments are fetched
        NotificationCenter.default.addObserver(self, selector: #selector(getNewCreatedAppointment), name: NSNotification.Name(rawValue: Constants.NOTIFICATIONS.NEW_APPOINTMENT_CREATED), object: nil)
    }
    
    @objc private func getFetchedAppointments(){
        filteredData = appointmentsModel.arrAppointments
    }
    
    @objc private func getNewCreatedAppointment() {
        filteredData.removeAll()
        appointmentsModel.arrAppointments.removeAll()
        
        appointmentsModel.getAllAppointments()
    }

}

//MARK:- Extension :- TableViewDataSource & Delegates
extension MyAppointmentsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if filteredData.count == 0 {
            return 1
        } else {
            return filteredData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if filteredData.count == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: NoDrFoundTableViewCell.reuseIdentifier) as! NoDrFoundTableViewCell
            cell.configureDescriptionLabelForNoAppointments()
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: AppointmentTableViewCell.reuseIdentifier) as! AppointmentTableViewCell
            cell.doctorNameLabel.text = "Dr. " +  (filteredData[indexPath.row].doctor?.name)!
            cell.doctorSpecialityLabel.text = filteredData[indexPath.row].doctor?.speciality
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if filteredData.count == 0 {
            return 314
        } else {
            return 145
        }
    }
    
}
