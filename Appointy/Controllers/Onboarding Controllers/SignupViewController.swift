//
//  SignupViewController.swift
//  Appointy
//
//  Created by Faiq on 12/05/2021.
//

import UIKit

class SignupViewController: UIViewController {
    
    //MARK:- IBOutlets
    @IBOutlet weak var fullNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var contactNumberTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var signUpBtn: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    
    //MARK:- Variables
    private let routes = Routes()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupElements()
        hideKeyboardWhenTappedAround()
    }
    
    //MARK:- IBActions
    @IBAction func signupBtnTapped(_ sender: UIButton) {
        //
        self.showLoadingSpinner()
        
        //validate fields
        let error = validateFields()

        if error != nil {
            //There's something wrong, show error message
            showError(error!)

        } else {
            //create cleaned version of the data
            let fullName = fullNameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let email = emailTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let contactNumber = contactNumberTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let password = passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let confirmPassword = confirmPasswordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)

            //Signup
            routes.signupPatient(fullName, email, contactNumber, password, confirmPassword) { (isSignedUp) in
                if isSignedUp {
                    DispatchQueue.main.async {
                        self.removeLoadingSpinner()
                        self.transitionToLoginScreen()
                    }
                } else {
                    DispatchQueue.main.async {
                        self.removeLoadingSpinner()
                        self.errorLabel.text = "Error Signing up. Make sure you have entered all field correct."
                        self.errorLabel.alpha = 1
                    }
                }
            }
        }

    }
    
    //MARK:- Helper Methods
    private func setupElements(){
        //hide the error label
        errorLabel.alpha = 0
        
        //styling text fields
        Utilities.styleTextField(fullNameTextField)
        Utilities.styleTextField(emailTextField)
        Utilities.styleTextField(contactNumberTextField)
        Utilities.styleTextField(passwordTextField)
        Utilities.styleTextField(confirmPasswordTextField)
        Utilities.styleFilledButton(signUpBtn)
    }
    
    /*
     Check fields and validate that the data is correct. If everything is correct, return nil. Otherwise return the error.
     */
    private func validateFields() -> String? {
        
        //check all that fields are nil
        if fullNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            contactNumberTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            confirmPasswordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            return "Please fill in all fields"
        }
        
        //check if password is secure
        let cleanedPassword = passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let cleanedPassword1 = confirmPasswordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if Utilities.isPasswordValid(cleanedPassword) == false {
            //password isn't secured
            return "Please make sure your password is at least 8 characters, contains a special character and a number."
        }
        
        if cleanedPassword != cleanedPassword1 {
            return "Your Password doesn't matched with your Confirm Password"
        }
        
        return nil
    }
    
    private func showError(_ message: String) {
        errorLabel.text = message
        errorLabel.alpha = 1
        
        DispatchQueue.main.async {
            self.removeLoadingSpinner()
        }
    }
    
    private func transitionToLoginScreen(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: Constants.STORYBOARD.SIGN_IN_CONTROLLER_ID)
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
