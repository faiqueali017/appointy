//
//  OnboardingViewController.swift
//  Appointy
//
//  Created by Faiq on 12/05/2021.
//

import UIKit

class OnboardingViewController: UIViewController {

    @IBOutlet weak var signupBtn: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var illustrationImageView: UIImageView!
    
    @IBOutlet weak var mainPictureViewBg: UIView!
    @IBOutlet weak var quoteViewBg: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupElements()
        animateIllustrationImageView()
        
        mainPictureViewBg.layer.cornerRadius = 15
        quoteViewBg.layer.cornerRadius = 15
    }
    
    //MARK:- Helper Methods
    private func setupElements(){
        //styling text fields
        Utilities.styleFilledButton(signupBtn)
        Utilities.styleHollowButton(loginBtn)
        
        //
        self.mainPictureViewBg.alpha = 0
        self.mainPictureViewBg.dropShadow(shadowOpacity: 0.3, shadowRadius: 13, shadowColor: UIColor.black.cgColor)
    }
    
    private func animateIllustrationImageView(){
        UIView.animate(withDuration: 1.5) {
            self.mainPictureViewBg.alpha = 1
        }
    }

}
