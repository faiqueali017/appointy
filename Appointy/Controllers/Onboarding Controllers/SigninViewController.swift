//
//  SigninViewController.swift
//  Appointy
//
//  Created by Faiq on 12/05/2021.
//

import UIKit

class SigninViewController: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var signInBtn: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    
    //MARK:- Variables
    private let routes = Routes()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupElements()
        hideKeyboardWhenTappedAround()
    }
    
    //MARK:- IBActions
    @IBAction func signinBtnTapped(_ sender: Any) {
        //
        self.showLoadingSpinner()
        
        //validate fields
        let error = validateFields()
        
        if error != nil {
            //There's something wrong, show error message
            showError(error!)
            
        } else {
            let email = emailTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let password = passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            
            //sign in
            routes.signinPatient(email, password) { (isSignedIn) in
                if isSignedIn {
                    DispatchQueue.main.async {
                        self.removeLoadingSpinner()
                        self.transitionToTabBarController()
                    }
                } else {
                    DispatchQueue.main.async {
                        self.removeLoadingSpinner()
                        self.errorLabel.text = "Error Signing in. Make sure to write correct email and password."
                        self.errorLabel.alpha = 1
                    }
                }
            }
        }
    }
    

    //MARK:- Helper Methods
    private func setupElements(){
        //hide the error label
        errorLabel.alpha = 0
        
        //styling text fields
        Utilities.styleTextField(emailTextField)
        Utilities.styleTextField(passwordTextField)
        Utilities.styleFilledButton(signInBtn)
    }
    
    /*
     Check fields and validate that the data is correct. If everything is correct, return nil. Otherwise return the error.
     */
    private func validateFields() -> String? {
        
        //check all that fields are nil
        if emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            return "Please fill in all fields"
        }
        
        //check if password is secure
        let cleanedPassword = passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if Utilities.isPasswordValid(cleanedPassword) == false {
            //password isn't secured
            return "Please make sure your password is at least 8 characters, contains a special character and a number."
        }
        
        return nil
    }
    
    private func showError(_ message: String) {
        errorLabel.text = message
        errorLabel.alpha = 1
        
        DispatchQueue.main.async {
            self.removeLoadingSpinner()
        }
    }
    
    private func transitionToTabBarController(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainTabBarContoller = storyboard.instantiateViewController(identifier: Constants.STORYBOARD.TAB_BAR_CONTROLLER_ID)
        
        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarContoller)
    }

}
